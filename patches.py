"""
Patches to the Slixmpp and IRC libraries.
"""

from slixmpp import *
from slixmpp.xmlstream.stanzabase import StanzaBase as Stanza
from slixmpp.componentxmpp import ComponentXMPP

import irc.client

import ssl

from time import time
from collections import defaultdict
from collections import namedtuple

from functools import wraps

from util import *

__all__ = []
__all__ += [ "isircchannel", "isbridgedircchannel", "isircnick", "IRCUser", "Mode", "LoggedMessage", "IRCChannel", "IRCClient"]
__all__ += ["MUCJID", "ComponentXMPP_", "MUC",]


def isircchannel(channel):
	"""
	check if 'room' is a valid IRC room identifier
	according to http://tools.ietf.org/html/rfc2811#section-2.1
	"""

	if isinstance(channel, JID):
		if isinstance(channel, MUCJID):
			room = channel.room
		else:
			room = channel.user
	return isinstance(channel,str) and channel[0] in ["#","&","!","+"] and all(c not in channel for c in [" ", "\x07", ",", ":"])

# ah! this is already defined!
# but the definition is slightly wrong: it doesn't check for alpha
# XXX this should also like, check for length limits?
irc.client.is_channel = isircchannel

def isircnick(nick):
	# XXX actually look up the spec and do this
	return True


def isbridgedircchannel(channel):
	# TODO: JID escaping provides a way to include & in the JID. http://xmpp.org/extensions/xep-0106.html
	if channel.startswith("$"): channel = channel.replace("$","&",1)
	return irc.client.is_channel(channel)




IRCUser = namedtuple('IRCUser', ['nick', 'user', 'host', 'realname', 'serverhost', 'idle_since', 'away', 'servername', 'channels'])

class Mode(set): pass

LoggedMessage = namedtuple('LoggedMessage', ['timestamp', 'nick', 'text']) # goes in IRCChannel.history[]

class IRCChannel:
	MAX_HISTORY = 1000
	
	def __init__(self):
		self.topic = None # either None or (topic, nick, timestamp) where topic is a string, nick is the person who set the topic, and timestamp is when it was set
			# note that because IRC splits the setting of topic into two commands ("topicinfo" and "currenttopic") there is a brief period where nick and timestamp may be null
		self.roster = set() # list of nicks in the room
		self.umode = defaultdict(Mode) # dict { nick :  modes } where modes is a set of single letter codes representing IRC permisisons, like set("O","v") for "op" and "voice"
		self.mode = Mode()
		
		self.history = [] #XXX this should be in b3.py. maybe in a LoggingIRCChannel or something.
		
		self.ready = asyncio.Event() # this is set when the channel has been joined, which is when endofnames fires.
			# without this, multiple clients joining at about the same time could cause the later clients to think the channel is joined and try to relay information from it when it's not!
	def __contains__(self, nick):
		
		if nick in self.roster:
			assert nick in self.umode
			return True
		return False
	def __iter__(self):
		return iter(self.roster)
	
	def log(self, nick, msg):
		"""
		record a message in the channel
		
		CHEATING: if msg starts with "/me " then it represents a ctcp ACTION command
		 this breaks typing, but it nicely falls in line with how XMPP does things and since I have no other pressures at the moment, the kludge it stays
		"""
		self.history.append(LoggedMessage(int(time()), nick, msg))
		self.history = self.history[-self.MAX_HISTORY:]

class IRCClient(irc.client.ServerConnection):
	"""
	Wrap irc.client for several purposes.
	
	- hide the obsoleted-by-asyncio-yet-still-causes-crashes-without-it Reactor class
	  - TODO: switch to irc3 which is on asyncio and therefore doesn't need this.
	- sniff more data than the parent class does
	  - room rosters
	    - user modes
	  - room topics
	  - room modes
	  ( the default stashes .real_nickname and .real_server_name, but those are not enough )
	- implement helper methods (like .back())
	- use SSL by default
	
	Basically, any patches to libirc that I found helpful or necessary5 go in here.
	This is distinct fro IRCHandler, which actually knows about bridging to XMPP.
	"""
	def __init__(self, *args, **kwargs):
		super().__init__(*args, reactor=irc.client.Reactor(), **kwargs)

		self.channels = {}
		for n in [f for f in dir(self) if f.startswith("_on_")]:
			def w(): #<-- this is to make a closure (because otherwise all event handlers are the last-processed)
				f = getattr(self, n)
				@wraps(f)
				def g(_, event): #<-- libirc assumes funny things about the event handlers so we do funny things to undo it ;)
					assert _ is self
					return f(event)
				return g
			self.add_global_handler(n[len("_on_"):], w())
	
	#### receivers
	# these simply log state, in addition to the logging of real_nickname and real_server_name that libirc already does
	# this means membership of room (rosters) and metadata about them




			# ASSUMPTION (I haven't read the RFC to check that this is correct):
			# a JOIN triggers in response:
			# JOIN
			# (CURRENTTOPIC; TOPICINFO) if a topic is set (else nothing)
			# NAMLIST*
			# ENDOFNAMES
			# So we are done joining a room when we see ENDOFNAMES
			# Which means if we set a temporary event handler to watch
			
	def _on_join(self, event):
		handle, _, _ = event.source.partition("!")
		channel = event.target
		assert isircchannel(channel)
		
		
		if handle == self.real_nickname:
			# this is us being told we've joined a channel
			
			if channel not in self.channels:
				# this is one we didn't ask for (the closest to this in XMPP is an invite)
				self.event("join_unexpected", event)
				self.channels[channel] = IRCChannel(); # create the new channel object
			
			assert len(self.channels[channel].roster) == 0
			# now: at this point we EXPECT to see this chain of events:
			#     (currenttopic,topicinfo)?,namreply*,endofnames
			# we leave the roster and topic blank for now, because they will be filled in by these events
		else:
			assert channel in self.channels, "We should not be notified about people joining channels that we are not a member of"
			self.channels[channel].roster.add(handle)
			self.channels[channel].umode[handle] # trigger creation
	
	def _on_mode(self, event):
		
		# command: mode, source: im.codemonkey.be, target: &bitlbee, arguments: ['+v', 'CharlieBreakcore']
		by, _, _ = event.source.partition("!")
		channel = event.target
		mode, who = event.arguments
		
		assert isircchannel(channel)
		assert isircnick(who)
		assert isircnick(by) or by == self.real_server_name
		
		assert len(mode) == 2, "Mode should be a two letter string. I think. maybe. I hope"
		state, mode = mode
		assert state in ['+','-']
		assert mode.isalpha() and len(mode) == 1
		
		assert channel in self.channels, "We should not be getting mode events for people in channels we aren't!"
		assert who in self.channels[channel].roster, "We should only get MODE events for users we've already seen JOIN or NAMREPLY into a channel"
		
		if state == "+":
			self.channels[channel].umode[who].add(mode)
		if state == "-":
			self.channels[channel].umode[who].remove(mode)
		
		
	def _on_nick(self, event):
		# command: nick, source: fdsfds!kousu@Clk-B5AD3370, target: masterhand, arguments: []
		was, _, _ = event.source.partition("!")
		becomes = event.target
		
		# rename was in every channel to becomes
		for C in self.channels.values():
			assert becomes not in C.roster, "No channel should have %(becomes)s" % locals()
			if was in C.roster:
				C.roster.add(becomes)
				C.roster.remove(was)
				C.umode[becomes] = C.umode.pop(was)
	
	def _on_part(self, event):
		handle, _, _ = event.source.partition("!")
		channel = event.target
		assert isircchannel(channel)

		assert handle in self.channels[channel].roster, "PART should only be sent on nicks that were in the channel, so either the server is broken or we're out of sync with it"
		
		self.channels[channel].roster.remove(handle)
		del self.channels[channel].umode[handle] #XXX this line should hide inside of the channel object
		
		if handle == self.real_nickname:
			# if it's *us* parting, then forget everything we know about the channel
			del self.channels[channel]
	
	def _on_kick(self, event):
		#FROM SERVER: :cn!kousu@Clk-7E04F526 KICK #test kousu :ha8ters
		#command: kick, source: cn!kousu@Clk-7E04F526, target: #test, arguments: ['kousu', 'ha8ters']

		by, _, _ = event.source.partition("!")
		channel = event.target
		who, reason = event.arguments
		assert isircchannel(channel)

		assert who in self.channels[channel].roster, "KICK should only be sent on nicks that were in the channel, so either the server is broken or we're out of sync with it"
		# XXX this code copypasted from _on_part
		
		self.channels[channel].roster.remove(who)
		del self.channels[channel].umode[who] #XXX this line should hide inside of the channel object

		if who == self.real_nickname:
			# if it's *us* parting, then forget everything we know about the channel
			del self.channels[channel]
	
	def _on_quit(self, event):
		# FROM SERVER: :nop_this!~nop_new@209.197.30.92 QUIT :Ping timeout: 260 seconds
		# command: quit, source: nop_this!~nop_new@209.197.30.92, target: None, arguments: ['Ping timeout: 260 seconds'], tags: None
		
		who, _, _ = event.source.partition("!")
		assert event.target is None
		msg = event.arguments[0] if event.arguments else None

		for channel in self.channels:
			if who in self.channels[channel]:
				
				# KLUDGE: translate QUITs to PARTs in *each channel*
				# without this then EITHER 
				#   there's no way for the XMPP bridge to know which channels to send <presence unavailable> for OR
				#   we cannot clean up self.channels[*].roster at this inner level
				# with this, a QUIT appears to be the client on the other end manually PARTing each channel individually, and THEN sending a QUIT
				self.event('part', irc.client.Event('part', event.source, channel, event.arguments))
			
		if who == self.real_nickname:
			logging.warn("irc://%s@%s got QUIT sent to emselves. Is this legal in the IRC spec? This probably should never happen right?" % (who, self.real_server_name))
			self.disconnect()



	# getting the /current/ topic is a two step process, annoyingly:
	# you end TOPIC, then receive either
        #command: currenttopic, source: galleon., target: kousu, arguments: ['#test', '']
        #command: topicinfo, source: galleon., target: kousu, arguments: ['#test', 'anna', '1456049007']
	# OR
	# command: notopic
        #TODO: and what happens if the topic hasn't been set yet?

	def _on_currenttopic(self, event):
		channel, topic = event.arguments

		assert event.source == self.real_server_name
		assert event.target == self.real_nickname

		self.channels[channel].topic = (topic, None, None) # XXX see above. we need to wait for topicinfo to fill in the rest

	def _on_topicinfo(self, event):
		channel, by, timestamp = event.arguments

		assert event.source == self.real_server_name
		assert event.target == self.real_nickname

		assert self.channels[channel].topic is not None, "currenttopic MUST have happened before topicinfo"
		topic, _, _ = self.channels[channel].topic
		self.channels[channel].topic = (topic, by, timestamp)

	def _on_topic(self, event):
		# command: topic, source: anna!kousu@Clk-7E04F526, target: #test, arguments: ['frell']
		handle, _, _ = event.source.partition("!")
		channel = event.target
		topic, = event.arguments

		assert isircnick(handle)
		assert isircchannel(channel)
		
		self.channels[channel].topic = (topic, handle, int(time()))

	def _on_notopic(self, event):
		channel, msg = event.arguments
		
		assert event.source == self.real_server_name
		assert event.target == self.real_nickname

		self.channels[channel].topic = None
	

	def _on_namreply(self, event):
		
		# command: namreply, source: galleon., target: kousu, arguments: ['=', '#test', 'kousu anna @saul ']

		_, channel, nicks = event.arguments
		assert isircchannel(channel)
		nicks = nicks.strip().split()
		
		for nick in nicks:
			mode = Mode()
			
			if nick.startswith("@"): # oper status is encoded inline in the name list.
				mode.add("o")
				nick = nick[1:]
			if nick.startswith("+"): # and voice status too
				mode.add("v")
				nick = nick[1:]
			
			self.channels[channel].roster.add(nick)
			self.channels[channel].umode[nick] = mode
		

	def _on_endofnames(self, event):
		"""
		

		"""
		assert event.source == self.real_server_name
		assert event.target == self.real_nickname
		channel, msg = event.arguments
		
		self.channels[channel].ready.set()
	
	def _on_pubmsg(self, event):
		if event.target in self.channels:
			msg, = event.arguments
			handle, _, _ = event.source.partition("!")
			self.channels[event.target].log(handle, msg)
	
	def _on_privmsg(self, event):
		if event.target in self.channels:
			msg, = event.arguments
			handle, _, _ = event.source.partition("!")
			self.channels[event.target].log(handle, msg)
	
	def _on_ctcp(self, event):
		if event.target in self.channels:
			cmd, msg = event.arguments
			handle, _, _ = event.source.partition("!")
			self.channels[event.target].log(handle, "/me " + msg)
	
	### 
	
	
	#### API

		
	def connect(self, *args, tls=True):
		super().connect(*args, connect_factory=irc.connection.Factory(wrapper=ssl.wrap_socket if tls else irc.connection.identity))
	
	### senders
	
	def join(self, channel):
		"postcondition: you can yield from self.channels[channel].ready.wait()"
		assert isircchannel(channel)
		
		if channel in self.channels: return
		self.channels[channel] = IRCChannel()
		super().join(channel)
	
	def away(self, msg):
		"funny. libirc doesn't define this"
		# http://tools.ietf.org/html/rfc2812#section-4.1 <-- spec
		if not msg:
			raise ValueError("away requires a message (or else the server will think you're trying to unaway yourself)")
		super().send_raw("AWAY :%s" % (msg,))
	
	def back(self):
		" return from away "
		# http://tools.ietf.org/html/rfc2812#section-4.1 <-- spec
		# "   message, or with no parameters, to remove the AWAY message. "
		super().send_raw("AWAY")

	def ison(self, nicks):
		if not nicks: #it's an error to ISON no nicks (and it's also pointless)
			raise ValueError("ISON requires non-empty list of nicks to probe.")
		super().ison(nicks)
	
	# XXX this is just privmsg()
	# TODO: or should it do pubmsg too!!! ah!
	def send(self, target, body): 
		" convert a multiline, possibly /me-ful, message to its equivalent IRC form "
		" target can either be #channel or user, distinguished simply by if it starts with a # or not"
		
		if target in self.channels:
			self.channels[target].log(self.real_nickname, body)

		if body.startswith("/me "):
			# /me commands: https://xmpp.org/extensions/xep-0245.html
			# yes it really is dumber than IRC
			send = self.action
			body = body.replace("/me ", "", 1)
		else:
			send = self.privmsg
		
		for line in lines(body):
			send(target, line)
	




def sanitize_stanza(stanza):
	" remove the list of banned XML "
	for k in stanza.keys():
		if isinstance(stanza[k], str):
			stanza[k] = sanitize_xml(stanza[k])




import slixmpp.roster.item
def handle_probe(self, presence):
	"monkey-patch slixmpp to NOT send presences just because someone is listed on a roster"
	"we handle presences manually"
	"i would rather not monkey-patch, but this is buried three classes deep in tidy OOP heaven where it's totally inaccessible to overriding via subclassing"
	#if self['from']: # <-- this *should* be "and self['status'] == 'available'" or something
	#	self.send_last_presence()
	if self['pending_out']:
		self.subscribe()
	if not self['from']:
		self._unsubscribed()
slixmpp.roster.item.RosterItem.handle_probe = handle_probe



class MUCJID(JID):
	"""
	JIDs on a XEP 0045 MUC have different semantics than regular JIDs
	because XEP 0045 is the worst.
	
	There are two kinds:
	- channel@conference.server.com - a room JID
	- channel@conference.server.com/user - an occupant JID
	  An occupant JID is backed by a "full" JID like user@server.com
	  but the room may hide this full JID from members
	  which is a totally unnecessary complication in the MUC spec:
	  anyone who wants anonymity on the web knows to make a throwaway account anyway.
	
	To make this clear, this class marks such JIDs apart and labels their
	components properly (e.g. instead of .user for the part before the "@", use .room)
	"""
	
	@property
	def room(self): return self._node
	user = None
	
	@property
	def nick(self): return self._resource
	resource = None
	
	@property
	def bare(self): return type(self)(super().bare)
	
	@property
	def full(self): return type(self)(super().full)
		

class ComponentXMPP_(ComponentXMPP):
	"""
	patches to ComponentXMPP. These
	"""
	def JID(self, user=None):
		"""
		Generate a JID for an entity on this component.
		"""
		if user:
			return JID("%s@%s" % (user, self.boundjid))
		else:
			return self.boundjid
	
	def send(self, stanza):
		if isinstance(stanza, Stanza):
			sanitize_stanza(stanza) # strip illegal XML
		super().send(stanza)


	def disconnect(self):
		# xmpp.disconnect() doesn't trigger session_end: that only triggers when the server kicks us off
		# which is ridiculous, because it's the same event either way
		# so do it manually
		# but be careful! session_end must not recurse back here!
		self.event('session_end')
		super().disconnect()


class MUC(ComponentXMPP_):
	"""
	TODO: this class should implement the server-side MUC protocol (slixmpp.plugins.xep_0045 is *client side* only, though that overlaps at the Stanzas with the server side.)
	right now it's just a placeholder.
	"""
	pass
	
	def __init__(self, *args, **kwargs):
		super().__init__(*args, **kwargs)
		
		# register a handler for "subject" changes
		self.register_handler(Callback('subject',
		                MatchXPath('{%s}message[@type="groupchat"]/{%s}subject' % (self.default_ns, self.default_ns)),
		                self._handle_subject))
		
		# and a handler for groupchat invites
		self.register_handler(Callback('invite',
				MatchXPath('{%s}message/{http://jabber.org/protocol/muc#user}x/{http://jabber.org/protocol/muc#user}invite' % (self.default_ns)),
				lambda stanza: self.event('invite', stanza)))
	
		
		
		self.register_plugin('xep_0030') # Service Discovery
		self.register_plugin('xep_0199') # XMPP Ping
		##self.register_plugin('xep_0045') # chatrooms (MUCs) # this only supports the client-side protocol, and 
		                                                      # further is BUGGY: it mangles messages, attaching
		                                                      # MUC data where no MUC data existed 
		#self.register_plugin('xep_0319') # idle times
	
	def _handle_subject(self, stanza):
		if not stanza['body']: #a groupchat message change only counts IF there's also no body;
			# I don't think it's possible to express this in pure XPath, but I'd love a patch that proves me wrong
			self.event('subject', stanza)
	
	def _handle_available(self, pres):
		" for MUCs, it is important to know *when a particular client* joins (becuase MUCs are stupid) "
		" we cannot fix this with an event handler: those happen *after* the roster is updated, and we lose the information about if *this* resource is new or not "
		" so we have to override here and hack it "
		try:
			self.roster[pres['to']][pres['from']].resources[pres['from'].resource]
		except KeyError:
			self.event('resource_got_online', pres)
		return super()._handle_available(pres)

	
	def send(self, stanza):
		# patch over a bug in the MUC spec: to send a <message> from a MUC, you must send with a resource included:
		# the server will *NOT* auto-relay these---it expects the MUC to track resources
		# whereas you can send direct messages without a resource easy as pie
		# (this is fucking stuuuuuuuuuuuuuuuuupid)
		# so, *if* the stanza is sent without a /resource, we relay it to *all* resources in the channel
		# (but if it is sent with one, we just pass it as normal)
		if isinstance(stanza, Message) and stanza['type'] == 'groupchat' and not stanza['to'].resource:
			jid = stanza['to'].bare
			room = stanza['from'].bare

			# this is the list of clients of jid's that have joined room
			# PLUS the target resource listed in the message, if specified
			clients = set(self.roster[room][jid].resources).union({stanza['to'].resource} if stanza['to'].resource else set())
			for client in clients:
				stanza['to'] = jid + "/" + client
				super().send(stanza)
			
			if not clients: #DEBUG: warn if the resource list is empty
				_m = "%(jid)s has no resources in %(room)s. Not relaying %(msg)r." % {"jid": jid, "room": room, "msg": stanza}
				logging.warn(_m)
				#raise ValueError(_m) #XXX this crashes the component because it interacts badly with Prosody. I'm not sure why other exceptions don't have this problem..
			return
		
		# fallback
		super().send(stanza)
	
	def JID(self, room=None, handle=None):
		"""
		Generate a JID for an entity on this component.
		
		Since this is a MUC server, there are two kinds of entities:
		- rooms
		- members in rooms
		If you specify just room, the first kind is created
		If you specify room and handle, the second kind is created.
		
		A MUCJID is returned instead of a JID.
		Separately from that, an entity in a non-anonymous room may have it's user-JID (user@server.com instead of room@conference.server.com/user) revealed.
		"""
		if room:
			if handle:
				return MUCJID("%s@%s/%s" % (room, self.boundjid, handle))
			else:
				return MUCJID("%s@%s" % (room, self.boundjid))
		else:
			return self.boundjid
