### util.py

__all__ = []

__all__.append('parse_netloc')

def parse_netloc(netloc, assume="host", default="0"):
	"if you assume it's a host, the default will be a port, and vice versa"
	
	if ":" in netloc:
		host, port = netloc.split(":",1)
	else:
		if assume == "host":
			host, port = netloc, default
		elif assume == "port":
			host, port = default, netloc
	
	port = int(port)
	return host, port
	


from functools import wraps
from traceback import format_exc
import logging
def catch_and_release(f):
	"""
	Display and propagate exceptions thrown by f().
	
	USE: Slixmpp silences exceptions sometimes. I don't think it's supposed to, but I am not interested in debugging it right now.
	"""
	@wraps(f)
	def g(*args, **kwargs):
		try:
			return f(*args, **kwargs)
		except:
			logging.debug(format_exc())
			with open("exc%d.log" % (time(),), "w") as err:
				print(format_exc(), file=err)
			raise
	return g

__all__.append('catch_and_release')

import asyncio
def coro_and_release(f):
	"""
	catch_and_release() ported to coroutines
	coroutines don't throw their exceptions when they are called; catch_and_release on a coroutine will just create the unstarted coroutine object and return it
	this actually executes the coroutine transparently
	"""
	@asyncio.coroutine
	@wraps(f)
	def g(*args, **kwargs):
		try:
			return (yield from f(*args, **kwargs))
		except:
			logging.debug(format_exc())
			# log exceptions to files individually, to make finding them later on easier
			# TODO: log the locals too (either via the inspect module or something else)
			with open("exc%d.log" % (time(),), "w") as err:
				print(format_exc(), file=err)
			raise
	return g

__all__.append('coro_and_release')

from io import StringIO
def lines(text):
	"""
	extract lines from text, in universal newline mode, *without* trailing newlines"
	
	USE: IRC cannot tolerate newlines in any command sent to it, so embedded newlines must be broken.
	"""
	return (line.rstrip("\r\n") for line in StringIO(text))

__all__.append('lines')



# WORKAROUND: prosody will kick a component off for sending characters illegal in XML (which is fair)
# but Slixmpp does not do any sort of validation on what you put into it, so we need to sanitize.
# Probably: Sleekxmp's XML generator should guarantee that what it generates is valid XML
# Probably: It shouldn't silently change the input, but it should warn.
# It's like encode() and decode(): when you put text into Slixmpp (or ElementTree?), you are asserting it's safe 
#
# This answer from http://stackoverflow.com/a/14323524/2898673
# the XML 1.1 spec does allow (almost) all Unicode chars, except for the ones that need escaping (which ElementTree handles)
# but XML 1.0 disallows some control characters, pretty much just cuz
# TODO: this should be done in a lower level hook, and it should sanitize all output.
import re
BAD_XML = re.compile("[^\x09\x0A\x0D\x20-\uD7FF\uE000-\uFFFD\u10000-\u10FFFF]")

def sanitize_xml(text):
	return BAD_XML.sub("", text)

__all__.append('sanitize_xml')


### Datetimes

# oh boy howdie.
# 
# XMPP uses ISO8601 dates: http://xmpp.org/extensions/xep-0082.html#sect-idp1124992
# and python cannot natively parse them:
# I *should* be able to use >>> datetime.datetime.strptime(, '%Y-%m-%dT%H:%M:%S%z') but at least on my Linux system %z doesn't seem to match anything
# disagreeing with the libC manpage
# "Note that this method does not handle time zones, so trying to parse 2004-06-03T12:34:56-0700 or 2004-06-03T12:34:56Z will fail. This is a fundamental limitation of time.strptime for which there is no easy workaround. " - https://wiki.python.org/moin/WorkingWithTime
# the options people seem to involve adding external libraries: http://stackoverflow.com/questions/12281975/convert-timestamps-with-offset-to-datetime-obj-using-strptime
#  - install dateutil
#  - install iso8601; which is unmaintained
#  - use xml.utils.iso8601.parse; which is deprecated?? at least, it's not in my distribution
#  - install pytz
# I feel like this is too small a thing to need a whole dependency for
# and indeed it is: time.timezone gives the timezone as integer seconds,
#          or datetime.timezone(datetime.timedelta(seconds=-time.timezone))) as a tzinfo object (see why below for a long explanation about why the - is needed)
# This chunk of code deals with this cruft sucking.

from datetime import *
from time import time


def tzlocal():
	" get the current timezone, as a tzinfo object, without using pytz or python-dateutil "
	# python's stdlib is so fucking shitty at time
	# I guess so is everyone
	# bah
	# When we move to the stars and have y2k again but where we need to adapt all our systems to account for relativity's time bending we're going to be really really sad
	
	# YES you literally need three operations to make this happen, plus the - patch
	
	tz = time.timezone
	
	# "2. Posix has GMT backwards." - https://regebro.wordpress.com/2007/12/18/python-and-time-zones-fighting-the-beast/
	# that is, time.timezone gives 18000 (+5 hours) for EST, which is actually UTC-5 --- and experimentally, this is even true on Waaaandows. Hooray for everyone being wrong together!
	# And of course, datetime.tzinfo uses the reverse definition, as proven by these lines:
	"""
	>>> # time of the first human footsteps on the Moon
	
	>>> h1 = dateutil.parser.parse("1969-07-21T02:56:15Z")
	>>> h2 = dateutil.parser.parse("1969-07-20T21:56:15-05:00")
	
	>>> h1
	datetime.datetime(1969, 7, 21, 2, 56, 15, tzinfo=tzutc())
	>>> h2
	datetime.datetime(1969, 7, 20, 21, 56, 15, tzinfo=tzoffset(None, -18000))
	>>> h1 == h2
	True
	
	>>> H1 = h1.replace(tzinfo=datetime.timezone.utc)
	>>> H2 = h2.replace(tzinfo=datetime.timezone(datetime.timedelta(hours=-5)))
	>>> H1
	datetime.datetime(1969, 7, 21, 2, 56, 15, tzinfo=datetime.timezone.utc)
	>>> H2
	datetime.datetime(1969, 7, 20, 21, 56, 15, tzinfo=datetime.timezone(datetime.timedelta(-1, 68400)))
	>>> H1 == H2
	True
	>>> 68400/60/60
	19.0 # -1 day + 19 hours == -5 hours; tzoffset and datetime.timezone(datetime.timedelta()) are slightly different
	
	# also, time.time() is a timedelta *from* a UTC time, and fromtimestamp() assumes this
	# e.g. at 9:15pm, EST:
	>>> datetime.datetime.fromtimestamp(t, datetime.timezone(datetime.timedelta(seconds=time.timezone)))
	datetime.datetime(2016, 3, 9, 7, 15, 50, 374095, tzinfo=datetime.timezone(datetime.timedelta(0, 18000))) # this jumped ahead 10 hours(!)
	>>> datetime.datetime.fromtimestamp(t, datetime.timezone(datetime.timedelta(seconds=-time.timezone)))
	datetime.datetime(2016, 3, 8, 21, 15, 50, 374095, tzinfo=datetime.timezone(datetime.timedelta(-1, 68400))) # this says 21:15, and the timedelta is negative
	"""
	# So, flip the definition:
	tz = -tz
	
	tz = datetime.timedelta(seconds=tz)
	tz = datetime.timezone(tz)
	return tz

# Now, the other thing we could do

def tznow():
	return datetime.datetime.now(tzlocal())


	
def datetime_parse(time):
	"""
	Parse an ISO8601 date into a datetime with a .tzinfo attached.
	
	That's the goal anyway. Given how much effort this is for other libraries I suspect I've missed something. Or the other libraries suck.
	"""
	#print("string:\t", time)
	time = time.strip()
	
	# extract the timezone
	# %Z is *supposed* to do this, but it simply doesn't work, and this is a known problem.
	# %Z works in strftime(), but not strptime(). Bitch!!
	# Though, when it works in strftime() it inserts "UTC" into the string.
	"""
	# %Z doesn't invert!
	>>> datetime.now(timezone.utc).strftime("%Y-%m-%dT%H:%M:%S%Z")
	'2016-03-09T05:17:31UTC+00:00'
	>>> datetime.strptime(datetime.now(timezone.utc).strftime("%Y-%m-%dT%H:%M:%S%Z"), "%Y-%m-%dT%H:%M:%S%Z")
	ValueError: unconverted data remains: +00:00
	>>> datetime.strptime(datetime.now().strftime("%Y-%m-%dT%H:%M:%S%Z"), "%Y-%m-%dT%H:%M:%S%Z")
	ValueError: time data '2016-03-09T00:15:39' does not match format '%Y-%m-%dT%H:%M:%S%Z'
	"""
	if time.endswith("Z"):
		# Zulu (aka UTC aka GMT) time
		time, tz = time[:-1], timezone.utc
	else:
		# Assumption: %Z means the last 6 chars are [+|-]HH:MM
		time, tz = time[:-6], time[-6:]
		sign, tz = tz[0], datetime.strptime(tz[1:], "%H:%M")
		tz = {'+': 1, '-': -1}[sign] * timedelta(0, tz.hour*60*60 + tz.minute*60)
		tz = timezone(tz)
	
	# parse the time
	try:
		time = datetime.strptime(time, "%Y-%m-%dT%H:%M:%S")
	except ValueError:
		try:
			time = datetime.strptime(time, "%Y-%m-%dT%H:%M:%S.%f")
		except ValueError:
			raise ValueError("Unable to parse time string '%s'" % (time,))
	
	# attach the timezone
	#print("parts:\t", time, tz)
	time = time.replace(tzinfo=tz)
	#print("parsed:\t", time)
	#print("unix:\t", time.timestamp())
	#print()
	return time

# unit tests:
# Datetime of the first human steps on the Moon, in different timezones.
# These should all give the same timestamp, since they're all the same time (assuming absolute newtonian time, which we know is false. siiigh.)
# The unix epoch is 1970-01-01 00:00:00, and the space age 'began' about half a year before that.
assert datetime_parse("1969-07-21T02:56:15Z").timestamp() == -14159025.0
# Example 3. Datetime of the first human steps on the Moon (Houston time)
assert datetime_parse("1969-07-20T21:56:15-05:00").timestamp() == -14159025.0
# a eastern timezone
assert datetime_parse("1969-07-21T09:56:15+07:00").timestamp() == -14159025.0
# newfoundland (with halfsies!)
assert datetime_parse("1969-07-20T23:26:15-03:30").timestamp() == -14159025.0

def timestamp_format(time):
	" ... "
	" we force the timestamp to be in zulu time "
	return datetime.fromtimestamp(time, timezone.utc).strftime("%Y-%m-%dT%H:%M:%S") + "Z"

# unit tests:
assert timestamp_format(-14159025.0) == "1969-07-21T02:56:15Z"


__all__ += ['tzlocal', 'tznow', 'datetime_parse', 'timestamp_format']
