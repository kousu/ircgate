#!/usr/bin/env python3
"""
Jabber IRC Transport of Intransigence: an opinionated XMPP->IRC bridge
======================================================================

Map

This server is unusual in the XMPP world in that it puts MUC ("Room"/"Occupant") and regular ("Bare"/"Full") JIDs on the same server.
 These JIDs *completely share syntax* but change the interpretation of the components
 See http://xmpp.org/extensions/xep-0045.html#terms
 
XMPP MUCs <--> IRC chatrooms (PRIVMSG #room)
XMPP MUC PMs <--> rewritten to regular PMs
XMPP PMs <--> IRC PMs (PRIVMSG user)
XMPP Presence <--> /away and /who and namelist

Now, we have to be careful to disambiguate the cases.
Also XMPP is more Stateful than I'd like to be dealing with, particularly around MUCs.


When our XMPP user is sending, we can see these kinds of messages:
<message type=chat to=channel@irc.server/user from=owner@server><body /> ---> PRIVMSG user body
  plus give warnings that you shouldn't use this address because it doesn't map to IRC cleanly
<message type=chat to=user@irc.server from=owner@server><body /> ---> PRIVMSG user body
   we can distinguish these two cases, I hope, because the second doesn't include a resource
<message type=groupchat to=channel@irc.server from=owner@server><body /> --> PRIVMSG #channel

Joining a MUC:
<presence to=channel@irc.server/handle from=owner@server><x xmlns="http://jabber.org/protocol/muc"></presence> #This one is confusing, because both 'from' and 'to' are the same person, and the server is expected to interpret this as a JOIN

Coming online:
<presence from=owner@server> status messages if desired </presence>
<presence from=owner@server type=unavailable> status messages if desired </presence> ---> /away (but only if *all* resources are away at the same time)

And when we receive from IRC, we see these kinds:
user PRIVMSG owner body --> <message type=chat to=owner@server from=user@irc.server><body />
user PRIVMSG #channel body --> <message type=groupchat to=channel@irc.server from=channel@irc.server/user><body /> # subtle!!!
JOIN #channel --> <presence ?????> ((this one was extremely tricky))


"""

"""
TODO


[ ] Registering UI:
 - [ ] cli style
 - [ ] jabber style (with inline forms and shit)

[ ] bug: irc_probe_presence reschedules for 5 seconds after it runs
 but it really shouldn't reschedule itself until 5 seconds after the last of its *consequences* runs i.e. 5 seconds after endofwhois.
  on real networks with lag involved, 5 seconds is *not* enough time, and it muddles up everything by making multiple requests at once. and i think this is somehow causing ISON to give up

[ ]  instead of auto authorizing, catch the subscribe hooks carefully and only set a "to" subscription

[ ] next: support groupchats
 [x] can I force-join people to rooms? because that would solve a lot of my problems --- and Slack's too 
  -- I can get the 'you're now in a room' stanza sent to pidgin, but pidgin ignores it. I wonder how Gajim handles it?
     I wonder how Conversations handles it?

[ ] carefully re-add opportunistic presence stanzas

[-] message receipts --- basically, if a message doesn't trigger nosuchnick, consider it a success?
  actually no, there's no way to know what receipt goes with what, so this will just confuse clients

[ ] vCards (filled in with WHOIS data)

[ ] Figure out if I can host an open transport 
 - i.e. can I just have irc.freenode.net.kousu.ca and let anyone's XMPP account come through that?


MUC TODO:
- [x] send final-presence on JOIN ~ALWAYS~
- [ ] figure out if xmpp.roster is tracking channels too --- if so, instead of relaying channel messages to everyone
   if not, we need to store the channel rosters manually. fucking MUC spec!!!
- send room presences on
 - new user join
 - room /away
 - room /quit
 -   XXX if the same user is in multiple 

"""

"""

INSTRUCTIONS:

pidgin:
 - add the transport to your server, or ask your admin to (links to prosody/ejabberd external component docs)
 - service disco
 - add the transport to your contact list
 - Log In / Log Out

gajim:
 - [ditto]
 -> set per contact status -> available or offline


You should save IRC channels you like to your [Bookmarks](), and set autojoin=1 and minimize=1
if your client doesn't support bookmarks, you will need to do this each time
 tho in pidgin and xabber, adding a conference saves it to your roster, which is almost as good

Tip: add an extra layer in the middle: stick znc in the middle, so that you don't lose history when you're offline
 Eventually bouncer-like features will probably blossom in this, but until then you can use znc can't you?

"""


"""


Idea: instead of waiting for the namelist to come in, lie:
 XMPP clients will say "____" has joined the chat, which is maybe not totally true, but it's really not that bad


	# first, let's get messaging working
	# and let's see if I can get messaging working
	# (get the server to at least *pass* MUC messages, even if Pidgin ignores them)
	


Further complicating matters is that XMPP components necessarily need to serve all users (since they only get one domain at a time)
so the bridge needs to track multiple users at once
Well, maybe I can, uh, make a couple of wrappers that basically curry over the user accounts:
 xmpp = XmppPseudoClient(self.xmpp, "juliet@montague.net")
 xmpp.send_message("channel@irc.server", "Forsooth, I die!", type="groupchat")
  --> xmpp.parent.send_message("channel@irc.server", "Forsooth, I die!", mfrom="juliet@montague.net", mtype="groupchat")
  





Weiiiiiiiiiird. Slack decided to map presence to +v (voice): https://get.slack.help/hc/en-us/articles/201727913-Connecting-to-Slack-over-IRC-and-XMPP
wtf?

TODO: remember to map voice -> role = somethingsomething (because some chatrooms)
and op -> role="moderator"
map Room Discovery http://xmpp.org/extensions/xep-0045.html#disco-rooms -> LIST http://tools.ietf.org/html/rfc2812#section-3.2.6
  giving back just the list of rooms is pretty easy, but mapping the occupant count and subject requires diving into a huge huge huge extension spec:
   http://xmpp.org/extensions/xep-0045.html#example-10
  unfortunately, to proxy back

"""



import asyncio

from b3 import *

import click
		
@click.command(help="""An XMPP to IRC gateway, as an XMPP external component.

SECRET: the password needed to register this component with the XMPP server.

IRC: The irc server to proxy to `irc_host[:port]`. If port is not given, defaults to 6697, IRC's SSL port.

What that means is that this is something you can plug into an XMPP server you control
as a minature sub-server, to offer XMPP accounts on that server the ability to proxy
themselves to an IRC network. XMPP features like presence, messaging and groupchat
are married to IRC features of /away, /query, and chatting on channels.

If you instead want to offer IRC clients access to your XMPP network, look into
Bitlbee or an (......).

By design, you as the server admin explicitly chooses which IRC server to bridge to.
Multiple IRC servers require multiple jitoi instances. This keeps the code relatively simple,
makes it hard to make yourself an open proxy, and means that the XMPP domain displayed
actually represents a real IRC domain (in stark and opinionated contrast to the XMPP tradition that subdivides the local part of a JID: user%ircserver@ircgateway.example.com)

The transport will be available at `irc_host` when seen from within your XMPP server.
If you have something else in mind, you can pick with `--domain transport_name`.  
Note that service discovery only finds items tucked under the same domain as the server being
queried from, which means you have to use a domain like `irc_host.example.com` if you want
your users to be able to find the transport.  But since no one actually uses service discovery
to do this, and even if they do it's once and only once, it's just as easy to write a blog
post or set a MOTD to inform users about the transports they can't disco', and then
you get the benefit of shorter, more verisimilitudical, addresses:
JohnSmith@facebook.com instead of JohnSmith@bitlbee-facebook.example.com


You should enabling [carbons](http://xmpp.org/extensions/xep-0280.html)
Carbons only affect PMs and ignore groupchats, but they mean that PMs relay to every
client and just generally make dealing with IRC less irritating.


Be careful not to accidentally stomp yourself. If your chat server is xmpp://example.com
don't also put up a component named example.com. This may seem obvious, but it could happen
by accident. If you run several services locally and just blindly substitute you might do
```
$ jitoi PASSWORD example.com
```
And the same for if you have multiple transports in parallel to the same server: make sure to disambiguate them.
E.g. by putting up xmpp://icq.example.com and xmpp://irc.example.com even if both are transports for servers on example.com.


Note: & is illegal in jabber addresses, so channels with them in their names
are transparently renamed with a $ symbol.
That is, though xmpp://#room@irc.freenode.net is irc://irc.freenode.net/#room
and most addresses map directly like that,
xmpp://$room@irc.freenode.net is irc://irc.freenode.net/&room instead.


Examples
--------

Suppose you have prosody installed. Add this to its config file:
```
component_ports = { 5347 }

Component "irc.freenode.net"
        component_secret = "iFO8EXjs"
Component "conference.irc.freenode"
        component_secret = "iFO8EXjs"
```

And run
```
$ jitoi iFO8EXjs irc.freenode.net
```

Jitoi then authenticates to Prosody with component password 'iFO8EXjs' 
at localhost:5347 and proxies communications between xmpp://irc.freenode.net and irc://irc.freenode.net:6697.

That means:

* xmpp://user@irc.freenode.net <-> irc://irc.freenode.net:6697/user

* xmpp://#room@conference.irc.freenode.net <-> irc://irc.freenode.net:6697/#room



Note: in the default configuration that xmpp server, xmpp://irc.freenode.net,
is a ghost.
 It is only visible *for accounts on the server the component is on*.
Your account romeo@hell.xxx will be able to talk to it, but juliet@balcony.net
will not, even if she knows the address it supposedly has, because balcony.net
will try to resolve an XMPP server on the real irc.freenode.net.
If Juliet's wet nurse installs jitoi in the same set up on balcony.net,
then Juliet will see xmpp://irc.freenode.net, but that will just be pointing
to the new jitoi.
 However, both should faithfully point through to the real irc.freenode.net,
 which means this should be a minor detail. The only noticeable effect is that
 Romeo's bridged contacts are tied to his home server.
 If he switches to romeo@privateinternetaccess.net he will no longer
 by able to see or use the bridges addresses he was used to.
 
If you want to make it into a public transport then you need to point
a working public DNS address at it and tell jitoi this address with --domain,
and tell your XMPP server the same.
TODO: verify this. TODO: document this.


Example: disable SSL, and deal with ejabberd's default component port being 8888 for some reason.

This creates contacts like:
friend@irc.freenode.net
#test@conference.irc.freenode.net
```
$ jitoi iF08EXjs -j 8888 -z irc.freenode.net:6667
```

Example: add ZNC as an extra layer of bouncey protection.
This makes contacts like
friend@znc
#test@conference.znc
```
$ znc --config
$ jitoi pasaf2j --domain znc localhost
```
""")
@click.argument("secret") #TODO: use type=password here?
@click.argument("irc")
@click.option("-j", "--jabber", default="5347", help="The address your XMPP servers listens on for external components upon. Note well that there is no encryption in the external component protocol, because XMPP can never quite get its act together, so this assumes the server is on localhost by default and you should at most give a port here. If it isn't on localhost, rather than specify its address, consider making an encrypted tunnel out of ssh or socat.")
@click.option("-d", "--domain", help="By default the transport masquerades on XMPP under the same hostname as IRC. This overrides that, letting you choose exactly what domain name to operate under.")
@click.option("-z", is_flag=True, help="Disable TLS (aka SSL) to the target IRC server. Before using this try (i) connecting on different ports (ii) asking your server admin to get a cert (free from StartSSL or LetsEncrypt!) (iii) crying (iv) not using IRC. ")
def main(secret, irc, jabber, domain, z, test_jid=None, test_nick=None, test_pass=None, test_user=None, test_autologin=False):
	logging.basicConfig(format="%(message)s")
	if __debug__:
		logging.getLogger().setLevel(logging.DEBUG)


	#TODO: warn if domain is invalid (e.g. has underscores of %s or other illegal chars)

	jabber = parse_netloc(jabber, assume="port", default="localhost")
	#WORKAROUND: slixmpp dies for some reason if told to connect to "localhost":
	#   the DNS resolver gets 127.0.0.1 back but it sets the port to 0, which makes the connection attempt spin in place
	#   and curiously, if given a real domain name, the resolve just flatly fails
	#   to get around that, we precompute the resolve for Slix
	import socket
	jabber = (socket.gethostbyname(jabber[0]), jabber[1])
	
	irc_host, irc_port = irc = parse_netloc(irc, assume="host", default="6697") #CAREFUL: irc must run first here because of dependencies
	irc = irc + (not z,) # record if we're using SSL or not
	component = domain if domain else irc_host

	# point the log file to disk as well
	import os
	logfile = logging.FileHandler("jitoi_%s_%d.log" % (component, os.getpid(),), "w")
	logfile.setLevel(logging.DEBUG)
	logfile.setFormatter(logging.Formatter("%(asctime)-15s %(message)s"))
	logging.getLogger().addHandler(logfile)

	with Bridge(component, jabber, secret, irc) as bridge:
		if __debug__ and (test_jid and test_nick and test_pass):
			#DEBUG: connect to a test IRC account at boot, so I don't need to trip it from the GUI all the time
			import os, socket
			bridge.register(test_jid, test_nick, test_pass, test_user)
			if test_autologin:
				bridge.users.add_event_handler('session_start', lambda *args: bridge.connect_for(test_jid))
		
		@asyncio.coroutine
		def timer_loop():
			"DEBUG: I suspect that under load, some combination of the setup makes the event loop itself hang, and then either IRC or XMPP die"
			"this tests if the event loop hangs by timing lag"
			import time
			last = time.time()
			while True:
				yield from asyncio.sleep(1)
				cur = time.time()
				logging.info("TIMER DELTA: %d", (cur - last))
				last = cur
		asyncio.Task(timer_loop())
		asyncio.get_event_loop().run_forever()

if __debug__:
	main = click.option("-a", "--test_autologin", is_flag=True)(click.option("--test_user")(
	click.option("--test_pass")(
	click.option("--test_nick", help="FOR SPEEDING UP DEBUGGING")(
	click.option("--test_jid")(main)))))
	


if __name__ == '__main__':
	main()
