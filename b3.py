#!/usr/bin/python3
# -*- coding: utf-8 -*-

# networking

import asyncio
import ssl

from slixmpp.xmlstream.handler import Callback
from slixmpp.xmlstream.matcher import MatchXPath
from slixmpp.plugins.xep_0319.stanza import Idle
#from slixmpp.stanza import * #CAN'T use this because making stanzas directly sets the xmlns wrong to "jabber:client" (and I don't know how to undo it!!)
from slixmpp import stanza, JID, ET
from slixmpp.plugins.xep_0045 import MUCPresence #no, we can't get this with self.plugins["xep_0045"]
from slixmpp.componentxmpp import ComponentXMPP

import irc.client


# utils

from copy import copy
from time import time
from datetime import datetime, timedelta

from functools import wraps
from itertools import count, chain

# project-local

from patches import *
from util import *

from cli import TransportCLI


# DEBUG
import logging
from pprint import pprint, pformat

# iudsfhdsuihfdsdsfsdsdfi be more py3k!!!!!!!
try:
	import bpython.pager
	bpython.pager.unicode = str
except ImportError:
	pass

## Bridge Utils

class JIDict(dict):
	"""
	A dict that indexes by JID objects, but only keying on the *bare* part (i.e. without the part after the /)
	so D[JID("joe@sauce.com")] == D[JID("joe@sauce.com/Conversations")] == D["joe@sauce.com/WoW-3242ef3f67h"] are equivalent.
	"""
	def __getitem__(self, k):
		k = JID(JID(k).bare)
		return super().__getitem__(k)
	def __setitem__(self, k, v):
		k = JID(JID(k).bare)
		return super().__setitem__(k, v)
	def __delitem__(self, k):
		k = JID(JID(k).bare)
		return super().__delitem__(k)

isircroom = isircchannel #XXX backcompat


def ircaddr(jid):
	"convert JID to either an IRC channel or an IRC nickname, depending on the type of JID it is (that is, whether it's JID or a MUCJID)"
	
	if isinstance(jid, MUCJID):
		# a message via a MUC room (this shouldn't ever happen in UsersHandler)
		if jid.nick:
			# for room-mediated PMs
			return jid.nick
		else:
			# for channel messages
			# '&' is illegal in XMPP rooms so we substitute $ for it; see ChannelsHandler.JID()
			# the .replace() here maps XMPP naming -> IRC naming
			channel = jid.room
			if channel.startswith("$"):
				channel = channel.replace("$","&",1)
			return channel
	else:
		# a direct message (this shouldn't ever happen in ChannelsHandler but will be the only one that can happen in UsersHandler)
		return jid.user

## Bridge metaprogramming

from types import MethodType as method
class AutoregisterEventHandlers:
	"""
	A mixin to automatically registers methods named "on_something" to handle the "something" event.
	"""
	def __init__(self, *args, **kwargs):
		super().__init__(*args, **kwargs)
		
		#logging.debug("Autoregistering handlers for %s" % (self,))
		for n, f in {n.replace("on_","",1): getattr(self, n) for n in dir(self) if n.startswith("on_")}.items():
			#logging.debug("%s.%s" % (self, f))
			self.add_event_handler(n, catch_and_release(f))
		#logging.debug("Done autoregistering handlers for %s" % (self,))


## Connection classes
# There's three parts (ignoring the metaclasses):
# - IRCHandler -- client to an IRC server (sends messages to irc.freenode.net)
# - UsersHandler -- XMPP user domain (handles messages to user@irc.kousu.ca)
# - ChannelsHandler -- XMPP conference domain (handles messages to room@conference.irc.kousu.ca)
# These all do most of their work routing through the Bridge class below, so in a way,
# they are all part of one big class, but it's just a lot more readable to split them up.




class IRCHandler(AutoregisterEventHandlers, IRCClient):
	"Extend IRCClient to work with the bridge; analogous to UsersHandler and ChannelsHandler"
	
	PRESENCE_POLL_PERIOD = 10  #in seconds: (minimum) time to wait between rounds of polling for IRC presence
	RECONNECT_PERIOD = 11      #in seconds: (minimum) time to wait before reconnecting, if the connection goes down unprovoked
	
	# this makes irc API compatible (enough) with the Slixmpp event API
	add_event_handler = lambda self, *args: super().add_global_handler(*args)
	#del_event_handler = lambda self, *args: super().remove_global_handler(*args) #XXX this is broken
	
	def add_event_handler(self, event, handler):
		#import bpython; bpython.embed(locals_ = locals())
		@wraps(handler)
		def g(connection, event):
			" libirc passes a redundant argument to its event handlers; it does not host the wisdom of javascript. "
			" also i don't like it's stupid event object. maybe i do. i don't know. sorry. "
			assert connection is self
			return handler(event)
		g.__wrapped__ = handler
		super().add_global_handler(event, g)
	
	def del_event_handler(self, event, handler):
		# decouverte la handler originale
		# XXX quoi de `event`?
		f = [f for n, f in self.handlers.items() if f.__wrapped__ is handler]
		# si trouve, detruire-le
		if f:
			super().remove_global_handler(f)
	
	def event(self, name, *data):
		# kludge together
		# TODO: rewrite all handlers to just take a single event object
		# this will be consistent, then, at least, with how the xmpp side works too
		if isinstance(data[0], irc.client.Event):
			data = copy(data[0])
			data.type = name
		else:
			data = irc.client.Event(name, data[0], data[1], data[2:])
		super()._handle_event(data)
	
	## UTILS
	
	@staticmethod
	def irc_mode_to_xmpp_role(umode):
		#XXX to probe (with WHOIS) for voice and away status! just generally this is borkity borkington
		# There's a small inconsistency in mapping between IRC's (op, voice) and XMPP's role
		# Roughly speaking XMPP has "moderator" == op+voice, "participant" == "voice", "visitor" = no voice (see the full table)
		#
		# IRC by default allows people to talk without having voice; it's only sometimes
		# XMPP by default denies people to talk without having "participant" or "moderator"
		# BUT the spec says '** An implementation MAY grant voice by default to visitors in unmoderated rooms.'
		# which means it's legal for us to pretend the IRC users are voiceless (if they are) and still pass messages from them
		# --- tho this might require sending special flags when sending the room's metadata. i dunno.
		
		assert isinstance(umode, Mode)
		voice = "v" in umode
		op = "o" in umode
		
		return "moderator" if op else ("participant" if voice else "visitor")
	
	
	## Core
	
	def __init__(self, bridge, jid, irc_server, handle, password=None, user=None, realname="~jitoi", *args, **kwargs):
		# handle unlabelled numeric events
		# these are events which libirc doesn't give names to and/or custom server extensions
		# we simply map them all into privnotices, so that they come out the transport's window
		# TODO: we should probably map *everything* that isn't already taken, which means going through irc.events
		# I'll leave that for another day
		for n in set(range(600)) - set(irc.events.numeric): # 600 is a generous upper bound: I know the numbers go up to 501 at least.
			setattr(self, "on_%d" % n, self.on_privnotice)

		super().__init__(*args, **kwargs)

		self.bridge = bridge
		self.jid = JID(jid) # this is the JID we are proxying for
		self.irc_server = irc_server
		self.nickname = handle #XXX this shadows the base class's
		self.password = password
		self.username = user # this one too!
		self.realname = realname
		
		self.ready = asyncio.Event() # flagged after WELCOME happens. Until this point it is illegal to send commands to IRC (but libirc is not smart enough to enforce this)
		self._presence_poller = None
	
	
	@asyncio.coroutine
	@coro_and_release
	def _probe_presence(self):
		"""
		poll the IRC server for presence
		
		IRC doesn't support as rich or as on-demand presence as XMPP.
		You will be told presence information about people in channels you're in,
		but not just about single users, because IRC doesn't have a concept of per-user rosters.
		The only reliable way to map XMPP roster presence to IRC presence is to poll for it in a loop.
		
		Algorithm:
		- poll with ISON, (which is a standard but non-mandatory IRC command)
		  - for failed ISONs, set "unavailable"
		  - else, for successful ISONs, poll with WHOIS
		    - temporarily memoorize any presence data that comes in
		    - on_endofwhois, format the presence data you have
		      - (show=["away"|None], status=["away message"|None] ) (type=available is implicit in these sorts of stanzas)
		      - the idle time, if the server sent thatback on WHOISing *everyone*
		
		TODO: look into the "WATCH" command which *does* give a per-USER roster. (but it's also per-session):
		 you log in, set a watch list (i.e. tell th servhen the server sends you messages when members on your watchlist change state
		 we can probably look for this in the FEATURES list, and still fall back on polling if it fails.
		 Disadvantage: you can't WATCH everyone, and the server will upperbound the number of people, so beyond that we still need to fall back on this
		WATCH example:
		TO SERVER: WATCH +saul
		FROM SERVER: :galleon. 604 kousu saul kousu Clk-7E04F526 1457943374 :is online
		command: 604, source: galleon., target: kousu, arguments: ['saul', 'kousu', 'Clk-7E04F526', '1457943374', 'is online']
		FROM SERVER: :galleon. 607 kousu :End of WATCH l
		command: 607, source: galleon., target: kousu, arguments: ['End of WATCH l']
		FROM SERVER: :galleon. 601 kousu saul kousu Clk-7E04F526 1457943672 :logged offline
		command: 601, source: galleon., target: kousu, arguments: ['saul', 'kousu', 'Clk-7E04F526', '1457943672', 'logged offline']
		FROM SERVER: :galleon. 600 kousu saul kousu Clk-7E04F526 1457943681 :logged online
		command: 600, source: galleon., target: kousu, arguments: ['saul', 'kousu', 'Clk-7E04F526', '1457943681', 'logged online']
		
		  notably: it *does not* capture away status, which means *we still* need to probe for that!
		

		
		Note: because this is a coroutine and all its parts are coroutines too,
		we are guaranteed that only one block of whois/ison probes can be in flight at once,
		which means we won't hammer the server even if we reduce PRESENCE_POLL_PERIOD
		"""
		
		try: #CancelledError
			logging.debug("_probe_presence: blocking until %s is ready", self)
			yield from self.ready.wait()
			
			# a cache
			# an IRCUser object mean online with the given information
			# missing means offline
			# NOTE: IRCUsers, on account of being namedtuples, have == defined for free
			cache = {}
			
			for i in count():
				#logging.debug("this is xmpp:%s's %dth presence poll to %s" % (self.jid,i,self))
				
				# extract the IRC-bridged contacts from the roster, and write them as just their IRC handle
				
				# grab the subset of our roster that we have subscribed to
				# (this leaves the set of people that we only have 'from' to not that *anyone* ever uses that feature of XMPP)
				#roster = (JID(friend) for friend in self.bridge.users.roster[self.jid] if self.bridge.users.roster[self.jid][friend]['to'])
				# WORKAROUND: slixmpp only records roster[presence['to']][presence['from']], not the reverse
				#             because, to be fair, it's not where the user's roster lives
				#             but when writing a transport, we must know the rosters of the users we are proxying for
				#     XXX make sure to sync this with [....]
				roster = (JID(friend) for friend in self.bridge.users.roster if self.bridge.users.roster[friend][self.jid]['from'] and JID(friend).user)
				#roster = list(roster); print(roster) # DEBUG
				#roster = (friend for friend in roster                                     # filter out pairs that have got_offline
				#                 if self.bridge.users.roster[friend][self.jid].resources) # notice that the pair is [friend][self.jid],
								 # because self.jid][friend].resources means the resources of freind that self can see,
								 # not the resources of self that friend can see
									  
				#roster = list(roster); print(roster) # DEBUG
				#print(self.bridge.users.roster[self.jid].resources)
				
				# method B: union the roster's of self.jid's friends
				#roster = (JID(friend) for friend in self.bridge.users.roster if self.jid in self.bridge.users.roster[friend])
				
				# filter out the transport and contacts that aren't on the transport (not that any of those should be possible if the xmpp server is behaving itself)
				roster = (friend for friend in roster if friend.domain == self.bridge.users.boundjid.bare and friend.user)
				
				# write them as their IRC handle
				roster = [friend.user for friend in roster]
				
				# ask the server about them
				#logging.debug("probing %s", roster)
				online = set((yield from self.ison(roster))) #weird: this needs double parens or python hits a syntax error. bug?
				#logging.debug("received: ison() = %s", online)
				
				# notice the case-insenitive comparisions
				assert set(o.lower() for o in online).issubset(set(r.lower() for r in roster)), "roster = %s, online = %s" % (roster, online)
				offline = set(r.lower() for r in roster) - set(o.lower() for o in online) #XXX does this need to be 

				# send presence results
				# it's important to do the whole roster in one loop together
				# because inside the loop we call whois() on the online nicks
				# but it's possible those online nicks will have gotten offline by the time we get to them
				
				# notice: we send all the offline presences first, then followed by whois'ing the online presences
				# but it's all done in one loop
				# this way, presence information is always fresh as possible
				# (via some nullifying shennanigans) so that we relay the ISON information as fresh as possible
				# and we also relay the whois info as fresh as possible

				if __debug__:
					# debugging is easier when things are reliably ordered
					offline = sorted(offline)
					online = sorted(online)

				@asyncio.coroutine
				def none(nick):
					"a coroutine that just returns None"
					yield

				for nick, profile in [(nick, none) for nick in offline] + [(nick, self.whois) for nick in online]:
					profile = yield from profile(nick)

					# SKIP duplicate <presences>
					# Note: we use profile == None to mean "offline"
					# In XMPP, the assumption is that if you haven't seen a presence that it's offline
					#  so if we haven't seen this person before, .get(nick,None) -> None which is the same as profile being "offline"
					# However, to make this work, you have to disable slixmpp.roster.item.RosterItem.handle_probe's blind sending of <presence> (which is equivalent to <presence type='available'>)
					# because otherwise at sign-on time, *every* bridged contact briefly (mentirially) appears online.
					if cache.get(nick,None) == profile: continue 
					
					if profile is not None:
						# they're online
						cache[nick] = profile              #now, update the cache, after we've checked it (it's safe to not update because == presences are identical presences because we used namedtuple which gave that for free)

						logging.debug("whois(%s) = %s" % (nick, pformat(profile)))
						P = self.bridge.users.Presence()
						P['from'] = self.bridge.users.JID(nick)
						P['to'] = self.jid
						
						if profile.away is not None: # careful: away status of "" is a legal away message. I think.
							P["show"] = "away"
							P["status"] = profile.away
						
						if profile.idle_since:
							# TODO: http://xmpp.org/extensions/xep-0012.html
							# TODO: http://xmpp.org/extensions/xep-0256.html
							idle = Idle()
							idle['since'] = datetime.fromtimestamp(profile.idle_since) #subtlety: Idle() intrinsically knows how to handle datetime objects, but only through this setter # XXX what timezone is this in?! does it matter?
							P.append(idle)

						#KLUDGE: avoid hammering the server with a gazillion WHOISes in a row. this *will* make our thing less accurate, but maybe it will help keep the connection up.
						# this can only happen in the second block of nicks (the online block), and only after a long whois instead of a short failure whois
						# which I think is a reasonable tradeoff
						yield from asyncio.sleep(1)
						P.send()
					else:
						# they're offline
						if nick in cache: del cache[nick]

						self.bridge.users.make_presence(pfrom=self.bridge.users.JID(nick), pto=self.jid, ptype="unavailable").send()
						yield from asyncio.sleep(0)
				
				# now sleep for a bit, before polling again
				yield from asyncio.sleep(self.PRESENCE_POLL_PERIOD)

		except asyncio.CancelledError:
			# this happen when the IRC connection goes down
			logging.debug("_probe_presence cancelled.")
			return
		
	
	def connect(self):
		
		assert not self.is_connected()
		self.ready.clear()
		
		super().connect(self.irc_server[0], self.irc_server[1], self.nickname, self.password, self.username, self.realname, tls=self.irc_server[2])
		assert self.is_connected()
		
		# da majixks: hook libirc's event system into asyncio
		# presumably when asyncio is more stable libirc will simply plug into it natively somehow
		# oh, or it won't because there's libirc3 and why go to the trouble?
		
		self.loop.add_reader(self.socket, self.process_data)
		
		# the poller coroutine which maps WHOIS to XMPP <presence> (because IRC doesn't support event-based presence, or if it does it's nonstandard extensions, so we must poll)
		assert self._presence_poller is None or self._presence_poller.done(), "there should not be a previous presence poller!!"
		self._presence_poller = asyncio.Task(self._probe_presence(), loop=self.loop)


		logging.debug("%r is connected: %d" %(self, self.connected))
		return self
	
	
	def disconnect(self, reason=""):
		" called whenever the connection goes down, either on our end (i.e. call this to shut down the connection) or the other " 
		" reconnect is an *extension* to the parent API; to disable, explicitly pass reconnect=False "

		logging.debug("irc.disconnect")
		if not self.connected:
			logging.warn("irc.disconnect() called on an already disconnected connection to %s", self.server)
			return

		# super().disconnect() destroys self.socket before calling .event('disconnect')
		# In order to handle this correctly we remove the reader
		# If we didn't do this, then a disconnect can cause the reader to spin unchecked
		# ---because select() on an EOF'd pipe just returns immediately---pegging the CPU.
		self.loop.remove_reader(self.socket)

		# for similar reasons, stop the presence prober before actually trying to disconnect
		self._presence_poller.cancel()

		self.ready.clear() # mark ourselves down

		# XMPP: inform the user whose connection just went down
		# 1) tell them
		self.bridge.users.send_message(self.jid, "* Disconnected from %s" % (self.real_server_name,) + (": %s" % (reason,) if reason else ""), mtype='chat')
		
		# 2) Offline all their contacts
		logging.info("Offlining %s's contacts" % (self.jid,))
		# go through the roster and offline everyone
		# (if you don't do this, clients don't recognize that the contacts bridged over the transport are gone)
		for friend in [f for f in self.bridge.users.roster if JID(f).user and JID(f).domain == self.bridge.users.boundjid.bare]:
			for user in list(self.bridge.users.roster[friend]): # list() counteracts "dictionary changed size during iteration"
				self.bridge.users.send_presence(pto=self.jid, pfrom=friend, ptype="unavailable")
		
		# 3) Kick them (with special code 332) from all MUCs they're in
		logging.debug("Removing %s from all rooms" % (self.jid,))
		# http://xmpp.org/extensions/xep-0045.html#destroyroom #<-- this is.. underspecified? and anyway, the room is still there, we just aren't in it and can't access it.
		# http://xmpp.org/extensions/xep-0045.html#registrar-statuscodes "332: Inform user that he or she is being removed from the room because the MUC service is being shut down"
		# Conversations doesn't handle this. UPDATE: fixed in https://github.com/siacs/Conversations/issues/1712
		for room in [r for r in self.bridge.channels.roster if self.jid in self.bridge.channels.roster[r] and MUCJID(r).room and MUCJID(r).domain == self.bridge.channels.boundjid.bare]:
			channel = ircaddr(MUCJID(room))
			logging.debug("Kicking bridged user %s off room %s aka channel %s" % (self.jid, room, channel))
			
			# 332 is the "service shutdown" code. It's like a kick---the client closes this channel and forgets its roster---
			# but without the socially nasty "you have been kicked!" message
			self.bridge.channels.make_muc_presence(channel, self.real_nickname, pto=self.jid, prole="none", paffiliation="none", ptype="unavailable", status_codes=[110, 332]).send()
		
		# Now, optionally queue a reconnect*
		# oh boy: so, inside libirc, there doesn't seem to be a distinction
		# between a disconnect requested by us and one caused by the server
		# because everywhere socket.error happens, there's an immediate call to self.disconnect()
		# admittedly most of the time you want to handle both cases identically
		# everywhere this happens though libirc always calls .disconnect("Connection reset by peer") OR .disconnect("Connection reset by peer.")
		# Three ideas:
		# 1) check for this string (janky, but effective)
		by_choice = not "Connection reset by peer" in reason 
		# 2) inspect self.socket to look for an error case: since super().disconnect() is happening *after*, our side of the socket 
		#  --> this doesn't work: a socket object (and this is part of the old BSD sockets design) doesn't have error state available
		#   the best we can do is look at ._closed, but that's not set on a socket error. the only way to tell is to try to do two
		#   maybe we could try to send an empty string? which should trigger an error if the socket has crashed, but not do anything if it's still up
		#by_choice = True
		#try:
		#	#self.socket.send(b"") #<-- this doesn't work because it gives false positives: the SSL wrapper, if used, is pissed off about sending empty strings
		#
		#	# send a command *twice*: because it's only on the second send that we'll receive the failure for the firs
		#	# ..or wait. no. that's not necessarily true, just usually...
		#	# note: we do this with socket.send instead of self.send_raw or self.who() because that would trigger infinite recursion because it calls into disconnect in that case
		#	self.socket.send(b"WHO\n")
		#	self.socket.send(b"WHO\n")
		#
		#	# maybe I can read the socket? but with a timeout of 0?  
		#except OSError:
		#	from traceback import format_exc
		#	logging.debug(format_exc())
		#	by_choice = False
		# XXX this actually doesn't work: yes, if the socket crashes we detect that, but the SSL object doesn't like being told to send b"" and for some reason self.who() 
		# is it a case of assuming things will fail too hard?
		# like, are there too many ways to succeed?
		# this should default to not reconnecting, and only 
		
		# 3) explicitly add reconnect as a param; have it trigger by default so that only in the place where we don't need it do we fail it.
		# I'm going with #2 for now, because I can't tell which functions *should* give reconnect explicitly.
		# scratch that: #2 doesn't work. there is literally no way to tell if you've hit EOF

		if not by_choice:
			# but if we are taken down because the connection died
			logging.debug("Queueing reconnection for %s" % (self,))
			@asyncio.coroutine
			@coro_and_release
			def reconnect():
				self.bridge.users.send_message(self.jid, "* Reconnecting in %d seconds." % (self.RECONNECT_PERIOD,), mfrom=self.bridge.users.JID(), mtype='chat')
				yield from asyncio.sleep(self.RECONNECT_PERIOD) # notice: this happens BEFORE, so not to hammer the server
				# but it doesn't happen in the loop because we need to ensure that at the time the loop is running we're not currently connected (e.g. if the user already did reconnect us)
				while not self.is_connected() and self is self.bridge.irc.get(self.jid, None):
					# We should also be the active IRC connection for the current user; if not, we're a straggler task and need to quit
					try:
						self.connect()
					except Exception as exc:
						# inform the user of the continuing problem
						self.bridge.users.send_message(self.jid, "%s while reconnecting." % (exc,), mfrom=self.bridge.users.JID(), mtype='chat')
					yield from asyncio.sleep(self.RECONNECT_PERIOD) # notice: this happens BEFORE, so not to hammer the server
			self._reconnect_task = asyncio.Task(reconnect(), loop=self.loop)
		
		super().disconnect(reason)
		

	
	## Handlers
	
	# Connecting
	
	
	def on_welcome(self, event):
		
		self.ready.set() #XXX this should be over in patches.py
		# TODO: wake up any coroutines waiting on this. there's a semaphorish thing for asyncio...
		self.event('privnotice', event) # KLUDGE: bridge the welcome message as if it was a motd message, because it's pretty similar
		
		# TODO: can we send presence for ourselves here? To say, yes, hi, you're online?
		# No, we can't: that would involve sending from a domain we don't control.
	
	# Hacky trick: on_privnotice is defined later in this block (because messaging should come after connecting)
	# but I want to alias all these connect-time events to it
	# So define an on_privnotice which will get overwritten later, but for now can be used
	on_privnotice = lambda self, event: self.on_privnotice(event)	 #<-- this is not actually an infinite loop, because on_privnotice is redefined later! aka mad hax
	on_yourhost = on_privnotice
	on_created = on_privnotice
	on_myinfo = on_privnotice
	on_luserclient = on_privnotice
	on_luserme = on_privnotice
	def on_n_local(self, event): # this one has useless duplicated information that no one cares about, so just extract the trailing message part and be done with it
		event = copy(event)
		event.arguments = event.arguments[-1:]
		self.event('privnotice', event)
	on_n_global = on_n_local
	on_motd = on_privnotice

	on_notexttosend = on_privnotice
	on_useronchannel = on_privnotice
	on_passwdmismatch = on_privnotice

	
	# Disconnecting

	def on_error(self, event):
		"if some sort of error happens, this event happens and then on_disconnect happens"
		# but *sometimes* on_disconnect happens without on_error first happening
		# e.g. packet loss, the server has a bug, we have a bug, etc
		# so we shouldn't rely on this

		assert event.source is None
		msg = event.target # wtf? bug in libirc?
		assert event.arguments == []

		# XXX the one useful thing we could do here is grab the message and supersede the reason on_disconnect will get, which will always just be "Connection reset by peer." because that's hardcoded in libirc
		logging.warn("%s went down due to %s." % (self, msg))



	# Presence (joining/parting rooms, whois)

	def on_invite(self, event):
		# "command: invite, source: anna!kousu@Clk-B5AD3370, target: kousu, arguments: ['#test']"
		by, _, _ = event.source.partition("!")
		
		who = event.target
		channel, = event.arguments
			
		assert isircchannel(channel)
		
		if who == self.real_nickname:
			# invite to us
			I = self.bridge.channels.make_muc_invite(channel, self.jid, self.bridge.users.JID(by).bare)
			self.bridge.channels.send(I)
		else:
			logging.warn("Received an invite that wasn't for us.")
		
	def on_join(self, event):
		#
		# now, there are three cases, unfortunately, for this:
		# - if we sent a JOIN to the server, and the server is replying with a JOIN
		# - if we are being told to JOIN a room (e.g. we're logging into a bouncer, or someone has invited us)
		# - someone else has joined a room, theoretically, one that we're in
		# these cases aren't ambiguous in regular IRC: both cause you to enter a room and start receiving messages from it
		# but because XMPP separates invites from success messages, we need to separate them
		# ... the trouble is how to detect the separation? do we just literally have to remember "yes I am trying to join room X"??
		# IF we asked to join a room, then and we get a reply back
		# so, in on_xmpp_join
		# or maybe what we do is remember if 
		# but then what if this
		
		# soooooo being told to join a room is not the XMPP way
		# the XMPP way is to store your conferences in your roster (actually, in this random extension protocol called Bookmarks, fff)
		# and mark them either auto-join or not
		# when a client (which understands bookmarks) learns about the rooms its supposed to be in, it sends joins
		# a client is never *told* to join a room
		# the closest we have
		# so I'm going to make a decision here: 
		#   assume all joins were triggered by the client -- if they weren't, the client can go ahead and ignore themt
		# some clients will lose some messages, but this is the least bull-in-china-shop option
		handle, _, _ = event.source.partition("!")
		channel = event.target
		assert isircroom(channel)
		
		# 'users' presence
		# XXX this should be calling whois() / using the presence cache; as this is, this will make people appear 'online' even if they are 'away' and if the presence poller jams (due to caching) it won't notice this
		# until I rearchitecture to do that, this is okay
		P = self.bridge.users.Presence()
		P['from'] = self.bridge.users.JID(handle)
		P['type'] = "available"
		P['to'] = self.jid
		P.send()


		# 'channels' presence
		if handle == self.real_nickname:
			# we have joined
			# we don't do anything just here: we expect this to be done within the on_got_online flow
			# keenly: we aren't supposed to send self-presence until we send the room roster
			# (though we can; doing that just means that everytime you join it appears that everyone else also joins at the same time)
			pass
		else:
			# someone else has joined the channel
			
			# XXX how do we probe them for their away status? or op status? or voice status?
			# XXX maybe the thing to do is queue the new handle onto the room roster, and extend the presence poller so that it also outputs MUC presences
			# --> ah but tricky: the poller means you might be able to join a channel and say something before the presence stanza comes through. hm.
			# so maybe do both: send a presence *now* with possibly incorrect info but at least a correct handle, then in a split second the presence poller will pick it up and correct it
			self.make_presence(channel, handle).send()

	def on_part(self, event):
		who, _, _ = event.source.partition("!")
		msg = event.arguments[0] if event.arguments else ""
		
		channel = event.target
		assert isircroom(channel)
		
		# 'users' presence
		# PART is only for leaving *channels*. It doesn't tell us when a user is offline... http://tools.ietf.org/html/rfc2812#section-3.2.2
		# so we have nothing to do here

		# 'channels' presence
		if who == self.real_nickname:
			# forget our knowledge of the state of the room
			self.bridge.channels.roster[self.bridge.channels.JID(channel)].reset()
		
			# clients will accept being told that their roomnick has been unavailabled
			# however, they will immediately try to reconnect, which is *not* what we want
			# so we fudge: a self-part becomes a kick, so keep them out until the user has a chance to look at it
			self.bridge.channels.make_kick(channel, who, by=channel, reason=("PART" + (": %s" % (msg,) if msg else "")), pto=self.jid).send()
			#self.bridge.channels.make_muc_presence(channel, who, pto=self.jid, prole="none", paffiliation="none", ptype="unavailable", status_codes=[321]).send()
		else:
			self.bridge.channels.make_muc_presence(channel, who, pto=self.jid, prole="none", paffiliation="none", ptype="unavailable").send()
		
	
	def on_kick(self, event):
		who, reason = event.arguments
		
		by, _, _ = event.source.partition("!")
		
		channel = event.target

		assert isircchannel(channel)
		assert isircnick(who)
		assert isircnick(by), "??? maybe this is sometimes false?"
		
		jid = self.bridge.users.JID(who) if who != self.real_nickname else self.jid #<-- TODO: factor this; self.JID(nick)
		
		# irc side:
		# FROM SERVER: :anna!kousu@Clk-7E04F526 KICK #test kousu :anna
		# command: kick, source: anna!kousu@Clk-7E04F526, target: #test, arguments: ['kousu', 'anna']
		
		# xmpp side:
		# http://xmpp.org/extensions/xep-0045.html#kick
		# TODO: wrap this into make_xmpp_presence_kick
		
		if who == self.real_nickname:
			# forget our knowledge of the state of the room
			# without this, we cannot rejoin properly because Slixmpp thinks it's already joined
			# so it never triggers got_online
			self.bridge.channels.roster[self.bridge.channels.JID(channel)].reset()
			# XXX ^ this is wrooooooooooooooong
			# this *shares rosters between users*. which is sketchy!!
		
		# relay the kick to our bridged account
		# note: this covers both *us* being kicked and *someone else*
		self.bridge.channels.make_kick(channel, who, by, reason, pto=self.jid).send()	
	
	
	# Two ways to handle in-channel aways:
	# - because an in-channel JID is a *different type* from a direct message JID (but uses identical syntax, motherfuckers)
	#   maybe it's simply easier to treat them totally separately: if we catch an in-channel IRC away, translate it to an in-channel away
	# - or maybe when we send direct-message JID aways, we also scan our roster and send the away to all the channels too
	# the latter is more reliable but also more bookkeeping. also theoretically it's possible for an IRC server to away you in one channel but not another...? (maybe? is it? i think the IRC RFC implies that away is a global status)
	
	# Here is an example flow os using WHOIS
	# each line is a *separate* event we need to catch
	# in some ways this using event handlers to go backwards
	"""
TO SERVER: WHOIS anna
FROM SERVER: :localhost.localdomain 311 kousu anna kousu Clk-7E04F526 * :~xchat
command: whoisuser, source: localhost.localdomain, target: kousu, arguments: ['Anna', 'kousu', 'Clk-7E04F526', '*', '~xchat']
FROM SERVER: :localhost.localdomain 319 kousu anna :@#test 
command: whoischannels, source: localhost.localdomain, target: kousu, arguments: ['Anna', '@#test ']
FROM SERVER: :localhost.localdomain 312 kousu anna localhost.localdomain :Kousu Test IRC Server
command: whoisserver, source: localhost.localdomain, target: kousu, arguments: ['Anna', 'localhost.localdomain', 'Kousu Test IRC Server']
FROM SERVER: :localhost.localdomain 671 kousu anna :is using a Secure Connection
command: 671, source: localhost.localdomain, target: kousu, arguments: ['Anna', 'is using a Secure Connection']
FROM SERVER: :localhost.localdomain 317 kousu anna 373 1454858141 :seconds idle, signon time
command: whoisidle, source: localhost.localdomain, target: kousu, arguments: ['Anna', '373', '1454858141', 'seconds idle, signon time']
FROM SERVER: :localhost.localdomain 318 kousu anna :End of /WHOIS list.
command: endofwhois, source: localhost.localdomain, target: kousu, arguments: ['Anna', 'End of /WHOIS list.']

		"""

	def on_quit(self, event):
		#FROM SERVER: :saul!kousu@Clk-B5AD3370 QUIT :Leaving
		#command: quit, source: saul!kousu@Clk-B5AD3370, target: None, arguments: ['Leaving']
		who, _, _ = event.source.partition("!")
		assert event.target == None
		msg, = event.arguments

		# 'users' presence
		P = self.bridge.users.Presence()
		P['from'] = self.bridge.users.JID(who)
		P['type'] = "unavailable"
		P['status'] = msg
		P['to'] = self.jid
		P.send()

		# 'channels' presence
		# We also need to walk the room list and remove the quitter from all rooms (that they were in)
		# but we do not do it here! there's a gordian knot between having the superclass cleanly track IRC state
		# and being able to know which rooms are parted when
		# which was cut by translating a QUIT to a PART for each room the quitter was in

	
	def on_mode(self, event):
		# command: mode, source: im.codemonkey.be, target: &bitlbee, arguments: ['+v', 'CharlieBreakcore']
		channel = event.target
		mode, who = event.arguments
		
		self.make_presence(channel, who).send()
	
	@catch_and_release
	def make_presence(self, channel, handle, reason=""):
		
		logging.debug("IRCHandler.make_presence(%s, %s, %s)", channel, handle, reason)
		assert isircroom(channel) or channel.startswith("$") and not "@" in str(channel) # XXX KLUDGE
		assert isircnick(handle)
		
		# assumption: the superclass's state tracking magics has already updated the appropriate umode
		if channel in self.channels and handle in self.channels[channel]:
			role = self.irc_mode_to_xmpp_role(self.channels[channel].umode[handle])
			type = "available"
		else:
			role = "none"
			type = "unavailable"
		
		jid = self.bridge.users.JID(handle) if handle != self.real_nickname else self.jid
		return self.bridge.channels.make_muc_presence(channel, handle, pto=self.jid, jid=jid, ptype=type, prole=role, pstatus=reason)


	@asyncio.coroutine
	@coro_and_release
	def ison(self, nicks):
		"""
		A coroutine which performs a ISON and waits for the result
		"""
		
		if not nicks:
			# there's no point to ISON an empty set (and in fact our superclass already cancels this..which means we'll hang, never receiving the return ISON event)
			return []
		
		def chunk(args, n):
			"""
			return a list of lists containing each string in args
			where the total length of each list (plus a separating space between each) is no more than len

			Use case: IRC limits commands to 255 characters, so commands with a lot of arguments crash.
			This is just like Unix's xargs(1)
			"""
			i = iter(args)
			cur = []
			c = -1 # -1 to counteract the initial +1 because there's no initial space. not that it matters much.
			for e in i:
				cur += [e]
				c += len(e)+1 #+1 for the separating space
				if c>=n:
					yield cur
					cur = []
					c = -1
			if cur:
				yield cur

		online_nicks = []
		received_ison = asyncio.Event()
		def ison(event):
			nonlocal online_nicks
			nicks, = event.arguments
			online_nicks += nicks.strip().split() # a space separated list of ASCII nicks!
			received_ison.set()
		self.add_event_handler('ison',ison)

		# this loop is because it's very easy to overflow the 255 char limit on IRC commands when using ISON
		# this is too ad hoc..there's probably lots of commands where this is an issue
		_done = []
		for _nicks in chunk(nicks, 240):
			super().ison(_nicks)
			yield from received_ison.wait()
			received_ison.clear()

			_done.extend(_nicks)
		assert len(_done) == len(nicks), "chunk() should make ALL the nicks happen"

		self.del_event_handler('ison', ison)
			
		#logging.debug("ison: received %s", online_nicks) 
		
		return online_nicks
	
	@asyncio.coroutine
	@coro_and_release
	def whois(self, nick):
		"""
		A coroutine which performs a WHOIS and waits for the result
		"""
		# send an IRC WHOIS command
		# XXX careful: this needs to *actually send the thing*, it can't itself be a coroutine
		super().whois(nick)
		
		# now, rig up a bunch of event handlers to catch the reply
		
		def onlyif(f):
			" shim: "
			" only trigger if the nick in the whois result message is the expected nick "
			def g(event):
				assert self.real_server_name == event.source, "WHOIS replies come 'from' the server itself." #this is unlike XMPP, where presence replies come 'from' the target user.
		
				if event.arguments[0].lower() == nick.lower(): # IRC nicks are case insensitive, though we want to preserve case for display purposes
					f(event)
			return g
		
		
		# these events can happen in any order so we set handlers for all simultaneously,
		# and only block for endofwhois.
		# we predefine their outputs to None, so if they don't happen then no foul
		
		user = host = realname = None
		@onlyif
		def whoisuser(event):
			assert event.type == "whoisuser"
			nonlocal user, host, realname
			_, user, host, _, realname = event.arguments
		
		away = None
		@onlyif
		def whoisaway(event): #NOTE: the event is actually just 'away'; we also receive it for trying to message offline users. but since that conflicts with the away variable, it gets a new name 
			assert event.type == "away"
			nonlocal away
			_, away = event.arguments
		
		channels = None
		@onlyif
		def whoischannels(event):
			assert event.type == "whoischannels"
			nonlocal channels
			_, channels = event.arguments
			channels = channels.strip().split()
		
		serverhost = servername = None
		@onlyif
		def whoisserver(event):
			assert event.type == "whoisserver"
			nonlocal serverhost, servername
			_, serverhost, servername = event.arguments
		
		currentnick = None
		@onlyif
		def whoisaccount(event):
			assert event.type == "whoisaccount"
			nonlocal currentnick
			nick, currentnick, msg = event.arguments
			

		idle_since = None # a Unix Epoch timestamp
		@onlyif
		def whoisidle(event):
			assert event.type == "whoisidle"
			nonlocal idle_since
			_, idle, *_ = event.arguments # the second argument is, on *some* servers, a signon time
			idle_since = int(time()) - int(idle) # this is idle *since*, which is what XMPP wants
			idle_since = idle_since//15*15 # round off to the nearest quarter minute
			# this mitigates the effects of clock drift. it's possible for client and servers' clocks to drift or for network lag to mess up the measurement
			#leading to a stuttering idle time which causes a stuttering presence stanza ("if you say he's been idle 27 seconds and it takes between 2 and 5 seconds for that packet to get to you
			# and also their clock is running on jupiter where time is slower by .99%, how long has he actually been idle?")

		
		
		# there are two ways out: no nick
		done = asyncio.Event()
		@onlyif
		def endofwhois(event):
			assert event.type == "endofwhois"
			done.set()
		
		offline = False
		@onlyif
		def nosuchnick(event):
			assert event.type == "nosuchnick"
			nonlocal offline
			offline = True
			done.set()
		
		self.add_event_handler('whoisuser', whoisuser)
		self.add_event_handler('whoischannels', whoischannels)
		self.add_event_handler('whoisserver', whoisserver)
		self.add_event_handler('whoisaccount', whoisaccount)
		self.add_event_handler('whoisidle', whoisidle)
		self.add_event_handler('away', whoisaway)
		self.add_event_handler('endofwhois', endofwhois)
		self.add_event_handler('nosuchnick', nosuchnick)
		self.del_event_handler('nosuchnick', self.on_nosuchnick) # temporarily disable the regular nosuchnick handler, because it sends messages to the user which would get annoying fast
		
		yield from done.wait()
		
		self.del_event_handler('whoisuser', whoisuser)
		self.del_event_handler('whoischannels', whoischannels)
		self.del_event_handler('whoisserver', whoisserver)
		self.del_event_handler('whoisaccount', whoisaccount)
		self.del_event_handler('whoisidle', whoisidle)
		self.del_event_handler('away', whoisaway)
		self.del_event_handler('endofwhois', endofwhois)
		self.del_event_handler('nosuchnick', nosuchnick)
		self.add_event_handler('nosuchnick', self.on_nosuchnick) #XXX dangerous! if two whoises run concurrently this could smash everything!
		
		if offline: return None
		return IRCUser(nick, user, host, realname, serverhost, idle_since, away, servername, channels)
	
	
	
	
	def on_nick(self, event):
		" when a user's nickname changes; note: we only get these for ourselves or for someone in a channel we're in "

		# command: nick, source: fdsfds!kousu@Clk-B5AD3370, target: masterhand, arguments: []
		was, _, _ = event.source.partition("!")
		becomes = event.target

		# we can be here for two reasons: our nickname has changed, or someone else's has
		if becomes == self.real_nickname: # since irc's built in handler processes this first, real_nickname has already changes at this point
			M = self.bridge.users.Message()
			M['to'] = self.jid
			M['from'] = self.bridge.users.boundjid
			M['body'] = "You are now known as %s" % (becomes,)
			M['type'] = "chat"
			M.send()


		
		# we need to send *two* XMPP messages to change a nick: one to offline the old and one to online the new
		# notice: we do all of this *even for ourselves*
		# if we change our own nick, the server informs us of success by sending a NICK in response
		# usually the full-JID presences won't matter, but in theory it should be possible to add your own IRC account to your XMPP account
		# and it definitely matters to send the updated nick for groupchats 
		
		# update 'users' presence
		P = self.bridge.users.Presence()
		P['to'] = self.jid
		
		P['from'] = self.bridge.users.JID(becomes)
		P.send()

		P['from'] = self.bridge.users.JID(was)
		P['type'] = "unavailable"
		P.send()
		

		# update 'channels' presence
		for channel in self.channels:
			if becomes not in self.channels[channel].roster: continue # only do people in the channel, of course (note: we use becomes because we assume the roster has already been updated by the superclass)
			logging.debug("%s -> %s in channel %s" % (was, becomes, channel))

			self_presence = becomes==self.real_nickname

			# <XEP 45> says we have to send <presence> twice to change a handle:
			# once, 'from' the old handle, "unavailable" and with code 303
			# twice, 'from' the new handle
			# this way clients that ignore the status codes at least pick up the presence dropping and rejoining, even if they don't realize it's the same person

			P = self.bridge.channels.make_muc_presence(channel, was, pto=self.jid, ptype="unavailable", prole="none", status_codes=[303] + ([110] if self_presence else []))
			# attach the data needed to mark this as a 'change' (303 marks it as a change, nick says who the change is to
			P.find("{http://jabber.org/protocol/muc#user}x/{http://jabber.org/protocol/muc#user}item").set("nick", becomes)
			P.send()
			
			# we *also* have to send presence a second time for the new handle
			# TODO: look up mode flags
			role = self.irc_mode_to_xmpp_role(self.channels[channel].umode[becomes])
			P = self.bridge.channels.make_muc_presence(channel, becomes, pto=self.jid, ptype="available", prole=role, status_codes=[110] if self_presence else [])
			P.send()
	

	def on_nicknameinuse(self, event):
		"""

		TO SERVER: NICK grow
		FROM SERVER: :galleon. 433 kousu grow :Nickname is already in use.
		command: nicknameinuse, source: galleon., target: kousu, arguments: ['grow', 'Nickname is already in use.']
		"""
		assert event.source == self.real_server_name
		assert event.target in [self.real_nickname, "*", "AUTH"]
		badnick, msg = event.arguments

		M = self.bridge.users.Message()
		M['to'] = self.jid
		M['from'] = self.bridge.users.boundjid
		M['body'] = "%s: %s" % tuple(event.arguments)
		M['type'] = "chat"
		M.send()



	on_notonchannel = on_privnotice

	
	"""
		bah!
		
		nosuchnick this is sent both for
		- a message
		- a whois
		- a user OR a channel
		
		and there's no way to tell just from the event which one it was;
		we need to track if we're in a whois state or not a whois state
		and then how do we handle getting multiple whoises to route at once??
		
		_probe_presence() does its best to avoid triggering this by only calling whois() on people ison() claimed were online,
		but that doesn't guarantee, especially with a large roster, that by the time we whois() someone they haven't gotten offline
	
		instead, in whois() we explicitly disable then re-renable this event handler
		"""
	on_nosuchnick = on_privnotice

	# Topics
	
	def on_topic(self, event):
		handle, _, _ = event.source.partition("!")
		channel = event.target
		topic, = event.arguments

		assert isircnick(handle)
		assert isircchannel(channel)
		
		self.bridge.channels.make_subject(self.bridge.channels.JID(channel), handle, topic, self.jid).send()
	

	# Messaging
		
	def on_privnotice(self, event):
		" send server NOTICES as messages from the transport itself "
		#assert self.real_nickname == event.target or event.target == "*" or event.target == "AUTH"
		handle, _, _ = event.source.partition("!")
		line = " ".join(event.arguments)

		if handle == self.real_server_name:
			# map messages from the server to messages from the transport
			handle = None
		
		self.bridge.users.send_message(self.jid, line, mfrom=self.bridge.users.JID(handle), mtype='chat')
		
	def on_pubmsg(self, event):
		"for PRIVMSG targetted at a channel"
		assert isircchannel(event.target)

		channel = event.target
		handle, _, _ = event.source.partition("!")
		msg, = event.arguments

		M = self.bridge.channels.Message()
		M['type'] = "groupchat"
		M['from'] = self.bridge.channels.JID(channel, handle)
		M['to'] = self.jid
		M['body'] = msg
		M.send()
	
	def on_privmsg(self, event):
		handle, _, _ = event.source.partition("!")
		msg, = event.arguments

		assert isircnick(event.target) and not isircchannel(event.target), "libirc ensures that privmsg is only for PRIVMSGs sent to nicks, not to those sent to channels"

		assert event.target == self.real_nickname, "We should only be able to receive private messages destined for us"

		if handle == self.real_server_name:
			# map messages from the server to messages from the transport
			# (but usually the server uses NOTICE instead)
			handle = None

		M = self.bridge.users.Message()
		M['type'] = "chat"
		M['from'] = self.bridge.users.JID(handle)
		M['to'] = self.jid
		M['body'] = msg
		M.send()

	def on_ctcp(self, event):
		cmd, msg = event.arguments

		# command: ctcp, source: anna!kousu@Clk-B5AD3370, target: #test, arguments: ['ACTION', 'breaks character']
		if cmd == "ACTION":
			handle, _, _ = event.source.partition("!")

			if event.target.startswith("#"):
				M = self.bridge.channels.Message()
				M['from'] = self.bridge.channels.JID(event.target, handle)
				M['type'] = 'groupchat'
			else:
				assert event.target == self.real_nickname, "If it's not to a channel, we should only receive it if it's to us"
				
				M = self.bridge.users.Message()
				M['from'] = self.bridge.users.JID(handle)
				M['type'] = 'chat'
			M['to'] = self.jid
			
			# https://xmpp.org/extensions/xep-0245.html
			M['body'] = "/me %s" % (msg,)
			
			M.send()
		else:
			logging.warn("unsupported CTCP command '%s'" % (cmd,))
	
	def on_cannotsendtochan(self, event):

		channel, msg = event.arguments
	
		# command: cannotsendtochan, source: localhost.localdomain, target: kousu, arguments: ['#test', 'You need voice (+v) (#test)']
		assert event.source == self.real_server_name		
		assert event.target == self.real_nickname
		
		# TODO: translate *all* unrecognized IRC events in this way
		# 
		
		# translate to XMPP
		assert isircroom(channel)
		handle = event.source #this is actually the server's name, but we're reporting an error to we make it look like it's coming from the server as a member of this channel
		room = self.bridge.channels.JID(channel, handle)
		
		M = self.bridge.user.Message()
		M['from'] = room
		M['type'] = "groupchat"
		M['body'] = msg
		M['to'] = self.jid
		M.send()
	
	
	### Sugar
	##
	
	@property
	def loop(self): return self.bridge.loop

	def __str__(self):
		TLS = self.bridge.irc_server[2] #TODO: is there some way to extract this from the libirc info?
		return "irc%s://%s@%s:%d/" % ("s" if TLS else "", self.real_nickname if self.real_nickname else self.nickname, self.real_server_name if self.real_server_name else self.server, self.port)



########################################33


class ChannelsHandler(AutoregisterEventHandlers, MUC, ):
	def __init__(self, bridge, *args, **kwargs):
		super().__init__(*args, **kwargs)
		self.bridge = bridge
		
		
		
		"""#DEBUG: replicate this:

<!-- Out Tue 09 Feb 2016 06:00:43 AM EST -->
<iq xmlns="jabber:client" to="general@conference.localhost" type="get" id="240">
<query xmlns="http://jabber.org/protocol/disco#info" />
</iq>

<!-- In Tue 09 Feb 2016 06:00:43 AM EST -->
<iq id='240' type='result' to='kousu@localhost/Gajim' from='general@conference.localhost'>
<query xmlns='http://jabber.org/protocol/disco#info'>
<identity type='text' name='general' category='conference'/>
<feature var='http://jabber.org/protocol/muc'/>
<feature var='muc_unsecured'/>
<feature var='muc_unmoderated'/>
<feature var='muc_open'/>
<feature var='muc_temporary'/>
<feature var='muc_public'/>
<feature var='muc_semianonymous'/>
<x type='result' xmlns='jabber:x:data'>
<field type='hidden' var='FORM_TYPE'>
<value>http://jabber.org/protocol/muc#roominfo</value>
</field>
<field type='text-single' label='Description' var='muc#roominfo_description'>
<value>
</value>
</field>
<field type='text-single' label='Number of occupants' var='muc#roominfo_occupants'>
<value>1</value>
</field>
</x>
</query>
</iq>

"""
		

		# interesting: you don't actually have to advertise that you support mucs to be able to join MUCs over your component
		self['xep_0030'].add_identity(category='conference',
                              itype='text',
                              name='Jitoi IRC Transport', #Jabber IRC Transport of Intransigence
                              jid=self.boundjid.full,
                              lang='no')
		self['xep_0030'].add_feature(feature='http://jabber.org/protocol/muc', jid=self.boundjid.full)     
		self['xep_0030'].add_feature(feature='muc_nonanonymous', jid=self.boundjid.full)
		

		# TODO:
		# mayyyyyyyyyyyyyyyyyyyyyyybe I should have *two* xmppstreams going:
		# one on "irc.localhost" and one on "rooms.irc.localhost"
		# that would remove all my ambiguity problems
		# but it requires more configuration at install time, and configuration at install time is realllllly annoying
		
		# TODO: not just the MUC, but rooms and occupants can be queried via service discovery
		# start here and read for 10 pages: http://xmpp.org/extensions/xep-0045.html#disco-rooms
		# this is how to translate "other rooms this user is on" 
		# an 'identity' is something that can be queried
		# an 'item' is something that can be attached as a child; it is not necessarily queryable
		# xep 30 is dumb: 
		# DEBUGGING: explicitly set some disco items on #test
		# TODO: disco needs to be dynamically added for rooms
		self['xep_0030'].add_item("%s@%s" % ("test", self.boundjid), ijid=self.boundjid)
		self['xep_0030'].add_identity(category='conference',
                              itype='text',
                              name="test",
                              jid=self.JID("test"))
		self['xep_0030'].add_feature(feature='http://jabber.org/protocol/muc', jid=self.JID("test"))
		self['xep_0030'].add_feature(feature='muc_nonanonymous', jid=self.JID("test"))
		self['xep_0030'].add_feature(feature='muc_unmoderated', jid=self.JID("test")) #this is like chanmod -m, and is required for Conversations to allow voiceless users to send messages
		# see more about chanmodes at http://docs.dal.net/docs/modes.html#2. +O is also a sort of chanmode..
		
	
	## Utils
	
	def JID(self, room=None, handle=None):
		" IRC -> XMPP room names, and possibly members-of-room names"
		" because & is illegal in JIDs, but is one of the four IRC room namespaces, we map XMPP $ <-> IRC &, in channels "
		if isinstance(room, MUCJID):
			room = room.room
		if room and room.startswith("&"):
			# XXX this could be done more specifically and therefore reliably
			room = room.replace("&", "$", 1)
		
		return super().JID(room, handle)
	
	def make_subject(self, room, by, subject, jid, timestamp=None):
		"a subject stanza is a message with no body: <message><subject>...</subject></message>"
		if timestamp: raise NotImplementedError("timestamp")
		
		M = self.Message()
		M['from'] = self.JID(room, by)
		M['to'] = jid
		M['type'] = "groupchat"
		
		# SUBTLETY: in IRC, the empty topic and no topic are distinct states.
		# Because of how sleekxmpp handles assignment, an empty topic will be written as a missing <subject> element, i.e. it won't get sent.
		# which would be misinterpreted. o we have
		if subject:
			M['subject'] = subject 
		else:
			M.append(ET.Element("subject")) #<-- go one level down to accomplish the goal
		
		return M
		
	def make_muc_presence(self, pchannel, phandle, pto=None, prole="participant", paffiliation="member", ptype=None, pshow=None, pstatus=None, jid=None, status_codes=[]):
		"""
		channel should be the bare JID of the channel like channel@conference.example.com
		handle should be the nickname of the user in the channel
		role and affiliation grant different permission levels (see below)
		jid, if given, is the bare JID of the (i.e. their real account name); if not given the user is anonymous (not that XMPP by itself should be trusted for anonymity)
		  note: in this bridge, all MUCs should be "non-anonymous" because the MUC handles are hard-coded to match the server handles (because that's how IRC works)
		self determines if this presence is for the current user -- informing them of their own handle (since their handle != their JID in the MUC spec);
		  this also signals that this is the end of the initial channel roster (clients are not supposed to display the room until it sees the special flags attached to this case)
		
		XXX this is server-side only since it hardcodes that 'from' is an occupant JID instead of a regular JID. BEWARE. TODO: generalize.
		
		
		TODO: instead of passing JID or not passing JID, just pass a "anonymous" flag, because I'm currently passing redundant information:
		 jid == "%s@%s" % (handle, self.xmpp.boundjid) == "%s@%s" % (handle, JID(channel).domain)
		
		TODO: type, show and status
		presence['show'] = random.choice(["", "away", "xa", "dnd", "chat", "available"])
		
		 TODO: there's 100 and 210 codes as well, which may be sent simultaneously
		 maybe instead of self I should have *codes ? ahh. i dunno.
		 dfoisjfsodadsifjsoifjdso?!??!?!
		
		According to http://xmpp.org/extensions/xep-0045.html#associations
		role can be
		- visitor
		- participant
		- moderator
		- None
		these correspond to -v (voiceless), +v (voiced) and +o (operator) in IRC
		most users will be participants, but a silent, read-only user is a 'visitor'
		
		affiliation can be
		- Owner
		- Admin
		- Member
		- Outcast
		- None
		'None' means the absence of an association, and the meaning of that is up to the implementation 
		but note that these are just strings. this bridge does not enforce permissions.
		instead, we relay all messages and leave enforcement to the remote IRC server.
		
		
		beware: this assumes that room@conference.channels.com/user is the same as user@channels.com
		which, personally, I think is how all sane transports should behave, and is also the assumption Conversations.im and Gajim make client-side
		the whole 'anonymous' feature of XEP 0045 is fucking stupid. *no one uses it* because you're only as anonymous as the server the MUC is on(!)
		which is hardly anonymous at all; if you want true anonymity you make a throwaway account, and you probably do it through tor.

		this also assumes you've got an IRC session it can query, which makes this code pretty...tied..too close.
		TODO: pull the core of this out to patches then override it and change the API so that it inspects
		"""

		logging.debug("make_muc_presence(%s, %s, pto=%s)" % (pchannel, phandle, pto))
		assert isbridgedircchannel(pchannel)
		
		# huh
		# using Presence() tags it with xmlns="jabber:client" and it's IMPOSSIBLE to remove, and then the server won't route the packet
		# but using BaseXMPP.Presence() doesn't do this because that copies BaseXMPP.default_ns over to the namespace
		# but this is a static method so we don't have that available.
		# argh !!!!!
		#
		# dirty fix: tweak the class and then put it back immediately
		# this is single threaded code so this is safe!
		
		#_xmlns, Presence.namespace = Presence.namespace, ""
		#presence = Presence()
		#Presence.namespace = _xmlns
		
		# wait, maybe this is what we're
		# I don't understand: BaseXMPP.Presence and BaseXMPP.make_presence say *nothing* about BaseXMPP.default_ns, which is how "jabber:client" gets passed in. wtf?!
		presence = self.Presence()
		presence['to'] = pto
		presence['type'] = ptype
		presence['show'] = pshow
		presence['status'] = pstatus
		
		presence['from'] = self.JID(pchannel, phandle) # the 'from' field in a groupchat presence stanza is really the "what user are we talking about" 
		# even though the presence is sort of "from" the channel
		
		# attach the <x xmlns="...muc" /> element which has important metadata for making clients behave themselves (it's actually impossible to use
		x = MUCPresence()
		
		if paffiliation: #<-- this field seems useless. if you care about affiliations just make the room non-anonymous and let people see your addresses. jesus h mohammed.
			x.setAffiliation(paffiliation)
		
		if prole:
			x.setRole(prole)

		if jid:
			x.setJid(jid)
		
		for code in status_codes:
			x.append(ET.Element('status', {'code': str(code)}))

		presence.append(x)
		
		return presence
	
	
	def make_muc_invite(self, channel, sto, sfrom='', reason=''):
		"""
		plugins.xep_0045.xep_0045.invite() is *supposed* to do this but it's wrong somehow: it just doesn't work. the clients ignore it.

		FAILS:: <message from="kousu@bridge.localhost" to="kousu@localhost"><x xmlns="http://jabber.org/protocol/muc#user"><invite to="kousu@bridge.localhost" /></x></message>
		WORKS:: <message from='coven@bridge.localhost' to='kousu@localhost'><x xmlns='http://jabber.org/protocol/muc#user'><invite from='coven@bridge.localhost'></invite></x></message>
		the difference between this and that are
		- this sets the *from* to be the room
		- this tags <invite> with 'from' instead of 'to'
		probably because the invite is missing the 'from' field, which makes the clients go "i don't know who this is" and drop it
		"""
		# this abuses the second half of the "Mediated Invitation" protocol to get people to join rooms they aren't even in
		# see http://xmpp.org/extensions/xep-0045.html#invite-mediated
		# we are allowed to
		
		msg = self.Message() ## a "Message"? Really? XMPP is a cowling vegetable.
		msg['from'] = self.JID(channel) #<-- NOTICE: this is the room you're inviting TO 
		msg['to'] = sto
		x = ET.Element('{http://jabber.org/protocol/muc#user}x')
		invite = ET.Element('{http://jabber.org/protocol/muc#user}invite', {'from': str(sfrom)})
		if reason:
			r = ET.Element('{http://jabber.org/protocol/muc#user}reason')
			r.text = reason
			invite.append(r)
		x.append(invite)
		msg.append(x)
		
		return msg


	def on_invite(self, invite):
		
		# TODO: wrap this in something that parses it into a Stanza object with happyhappy convenient accessors
		mucfrom = JID(invite.get("from"))
		mucto = JID(invite.get("to"))
		i = invite.find("{http://jabber.org/protocol/muc#user}x/{http://jabber.org/protocol/muc#user}invite")
		invitefrom = JID(i.get("from"))
		inviteto = JID(i.get("to"))
		
		# under section 7.8.2 Mediated Invitation:
		# now, the way to parse an invite is dependent on which side you're on:
		# if (you're a client and sending) or (you're the MUC server and receiving) then:
		# - mucfrom is a full JID of the client (tho probably a bare JID is okay here too)
		# - mucto is a bare JID containing the room
		# - invitefrom is missing
		# - inviteto is the bare JID of the target
		# if (you're a client and receiving) or (server and sending):
		# - mucfrom is the bare JID of the room
		# - mucto is the bare JID of the target
		# - invitefrom is the full JID of the sender
		# - inviteto is missing
		# notice how the values *rotate*. it's not just that from and to get swapped:
		# mucfrom -> invitefrom
		# mucto -> mucfrom
		# inviteto -> mucto
		# invitefrom -> inviteto
		# really they should be in separate #subnamespaces
		# (this spec is terrible because of all the implicitness in it, where the point of XML was to be super explicit about every damn thing)
		
		# since here we're a server and receiving...
		# mucfrom is the sender, so we need to look up self.irc[mucfrom.bare]
		# we extract the room from mucto
		# we extract the nick fro inviteto
		by = mucfrom
		who = inviteto
		room = mucto
		
		
		assert str(invitefrom) == "", "invitefrom should be empty"
		
		
		irc = self.bridge.irc[by.bare]
		
		assert who.domain == str(self.bridge.users.boundjid), "check the domain is correct. the server shouldn't pass us stanzas not for us, but it doesn't hurt to be paranoid"
		assert room.domain == str(self.boundjid), "check the domain is correct. the server shouldn't pass us stanzas not for us, but it doesn't hurt to be paranoid"
		
		# convert to irc syntax
		who = who.user
		channel = ircaddr(MUCJID(room))
		
		irc.invite(who, channel)
		
	

	
	def make_kick(self, channel, handle, by=None, reason=None, pto=None):
		
		logging.debug("make_kick(%s, %s, %s, %s, %s)"% (channel, handle, by, pto, reason))
		
		# XML is so finicky
		# Inspect this carefully:
		"""
<presence
    from='harfleur@chat.shakespeare.lit/pistol'
    to='pistol@shakespeare.lit/harfleur'
    type='unavailable'>
  <x xmlns='http://jabber.org/protocol/muc#user'>
    <item affiliation='none' role='none'>
      <actor nick='Fluellen'/>
      <reason>Avaunt, you cullion!</reason>
    </item>
    <status code='307'/>
  </x>
</presence>
		"""
		
		# Example 90: Service Kicks removed Occupant
		# http://xmpp.org/extensions/xep-0045.html#kick
		# we handle 89 and 91 in xmpp.on_kick()
		# and we don't handle 92 informing the rest of the occupants, because the other occupants aren't XMPP
		# (well, they might also be bridged, but they'll be informed indirectly via IRC)
		# curiously, we *don't* put code 110 here.
		P = self.bridge.channels.make_muc_presence(channel, handle, pto=pto, prole="none", ptype="unavailable", status_codes=[307])
		
		# Now, attach by and reason
		# Note: MUCPresence.getXMLItem() apparently grabs the <item/> tag
		# but we have to generate the <status> tag all by ourselves
		# ugh
		# a proper XMPP library would mean the only time I have to think about XML (besides debugging) is when I'm registering handlers that know about different namespaces
		# otherwise I should just say "I want this feature and that feature" and it should figure itself out for me. bah.
		# there is much work to be done here.

		MP = P.find("{http://jabber.org/protocol/muc#user}x") # get the MUCPresence object #XXX EXCEPT IT DOESN'T. IT GETS AN ET.Element THAT WAS GENERATED FROM THE MUCPresence. Apparently the MUCPresence is now gone, along with all its helper methods. god fucking dammit.
		if by:
			MP.find("{http://jabber.org/protocol/muc#user}item").append(ET.Element("actor", {'nick': by}))
		
		if reason:
			# FUCK YOUUUUU ELEMENTTREEEEEE. I should be able to one-liner this. but I can't, because the cntent is a subelement, not an attr.
			r = ET.Element("reason"); r.text = reason
			MP.find("{http://jabber.org/protocol/muc#user}item").append(r)
		return P
	
	## Handlers
	
	# Connecting

	def on_session_end(self, _):
		self.loop.stop()

	# Presence
	
	# I want to be able to subscribe to the transport, but not to anyone else
	# how do I do that?
	# maybe I can force the roster to only have self.boundjid in it somehow?
	# maybe I implement on_presence_xxxx for all the possible presences such that it behaves itself?
	def on_presence(self, stanza):
		if "subscribe" in stanza['type']: return

		if stanza['to'] == self.boundjid:
			stanza.reply().send()
		
	def on_presence_probe(self, stanza):
		if stanza['to'] == self.boundjid:
			# the bridge itself is always online
			r = stanza.reply()
			r['type'] = "available"
			r.send()

	def on_presence_available(self, stanza):
		irc = self.bridge.irc[stanza['from'].bare]

		occupant = MUCJID(stanza['to'])
		channel = ircaddr(MUCJID(occupant.bare)) #ugh
		nick = occupant.nick

		if nick != irc.real_nickname:
			# change nick name!
			# we are careful to only do this on channels we are already in
			# http://xmpp.org/extensions/xep-0045.html#changenick
			# XXX there's no way to tell apart a "join" from a "change"
			# I want this behaviour:
			# - on join, I force your nick to match your IRC nick (because your client might be making stupid assumptions)
			# - on change, I accept your change
			# But both stanzas are identical. The only way to tell is if this happens before or after resource_got_online
			# but in a single flow this *always* happens before resource_got_online so there's no way to tell!
			# TODO: come back to this, and figure out if this is possible
			# in the meantime, there's /nick
			pass
			#irc.nick(occupant.nick)

	
	@catch_and_release
	def on_resource_got_online(self, stanza):
		"""
		happens whenever a receiving JID (i.e. a room, since this is the conference thing) sees a JID for the first time
		
		In the case of MUCs, this is when a particular client joins a particular room (since in XMPP it's *clients* that are joined to rooms, not JIDs, or in XMPP jargon full JIDs not bare JIDs; this is very different than direct messages, which are to a bare JID usually)
		since in IRC, each IRC nick has exactly one IRC client behind it (barring hacks, like this)
		we need to do fanout here: if this is the first time this bare JID has joined the room, then we have to irc.join
		otherwise, we replay cached information
		"""
		
		# be enthusiastic about connecting
		if stanza['from'].bare in self.bridge._registrations:
			# if registered: use the registration
			# TODO: restructure this to be more pseudo-anon friendly
			self.bridge.connect_for(stanza['from'])
		else:
			# allow pseudoanonymous login
			# this *only* works with MUCs because non-MUCs don't give any way to imply a nickname
			self.bridge.connect_pseudoanon(stanza['from'], MUCJID(stanza['to']).nick)
		
		asyncio.Task(self.join(stanza['from'], MUCJID(stanza['to']).bare, MUCJID(stanza['to']).nick, stanza.find("{http://jabber.org/protocol/muc}x/{http://jabber.org/protocol/muc}history")), loop=self.loop)
		
	
	@asyncio.coroutine
	@coro_and_release
	def join(self, client, room, requested_nick=None, history_filter=None):
		"The process for joining a particular client to a particular channel"

		# this prevents live messages being sent until we're ready for them (see below)
		_client = self.roster[room][client.bare].resources.pop(client.resource)

		# wait here for the IRC object to come up!
		irc = self.bridge.irc[client]
		yield from irc.ready.wait()

		channel = ircaddr(room)
		if channel not in irc.channels or not irc.channels[channel].ready.is_set():
			# we need to go through a join cycle
			irc.join(channel)
			assert channel in irc.channels, "enforce irc.join()'s postcondition"
			
		# now, whether we sent the join or not, wait until the channel is ready for us
		assert channel in irc.channels
		try:
			yield from asyncio.wait_for(irc.channels[channel].ready.wait(), 5)
		except asyncio.TimeoutError:
			# the RFC says
			# "If a JOIN is successful, the user receives a JOIN message as
			# confirmation and is then sent the channel's topic (using RPL_TOPIC) and
			# the list of users who are on the channel (using RPL_NAMREPLY), which
			# MUST include the user joining."
			# yet this isn't always true. At least: on Freenode, if you own a channel (i.e. it's registered with chanserv) and you create it (i.e. join it when no on is in it)
			# then instead of getting NAMREPLY you get MODE +o Chanserv; MODE -o yournick
			# Since we use NAMES to recognize when we have joined a channel successfully
			# we will hang forever until we get NAMREPLY
			# so: if we don't get NAMREPLY in 5 seconds, trigger it anyway
			# (if there's lag, this will trigger two NAMREPLYs, but the second one will just be ignored)
			# XXX this should probably be inside of irc.join()
			irc.names(channel)
			yield from irc.channels[channel].ready.wait()
		
		# 1) send room roster
		
		room = self.bridge.channels.JID(channel)
		
		assert irc.real_nickname in irc.channels[channel], "Our current nickname should be in %s's roster" % (channel,)
		
		anonymous = False # on our bridge, all rooms are "non-anonymous" (because MUC anonymity is a complication I don't want to deal with and which doesn't even make sense in the context of IRC)
		
		# we need to push *ourselves* to the end of the XMPP userlist, so we take ourselves out.
		# because the MUC spec, in a fit of stupidity, added a stateful implicit signal that
		# the user marked with <status code=110> is also
		# a) the handle of the receiving user
		# b) the last user that was in the channel at join time (any presences that follow are displayed as "XXX joined the room")
		# why did it do that? jussst to be difficult, surely.
		_nicks = list(set(irc.channels[channel].roster) - {irc.real_nickname}) + [irc.real_nickname]
		for nick in _nicks:
			status_codes = []
			
			if not anonymous:
				jid = self.bridge.users.JID(nick) if nick != irc.real_nickname else irc.jid #<-- TODO: factor
			else:
				jid = None
				
			role = irc.irc_mode_to_xmpp_role(irc.channels[channel].umode[nick])

			if nick == irc.real_nickname: # aka the last presence aka the self-presence
				status_codes+=[110]
				#if not anonymous:
					#status_codes+=[100] #"the room is non-anonymous" (duplicated info: can also be gotten by disco'ing the room)
				# --> this is actually irritating: most clients ignore it, those that don't say "Any member is allowed to see your full JID" which is actually a complete lie: they see your hostname and irc realname fields and IRC nickname, not your JID
				
				# obscure status code: the room changed your nick on you because you're an idiot
				if requested_nick and irc.real_nickname != requested_nick:
					# if the user has tried to join with a mismatching nick, quietly change it on them
					status_codes += [210] # "the room has changed your nick"
			
				if client.resource.lower() == "conversations":
					# WORKAROUND: Conversations helpfully assumes voiceless means
					# you shouldn't be able to type at all, but in an unmoderated
					# room this isn't true!
					# https://github.com/siacs/Conversations/issues/1710
					# in this case, if they are voiceless, lie to them and promote them
					# (if they're /actually/ voiceless, the room will take care of silencing them anyway)
					if role == "visitor":
						role = "participant"
			
			self.bridge.channels.make_muc_presence(channel, nick,
				pto=client, jid=jid, prole=role,
				status_codes=status_codes).send()

			yield from asyncio.sleep(0) # be kind to other coroutines (like, say, the IRC handlers and other simultaneous room joins)
		
		
		# 2) send room Subject (if set)
		if irc.channels[channel].topic:
			self.make_subject(room, irc.channels[channel].topic[1], irc.channels[channel].topic[0], client).send()
		
		# 3) Send history
		#logging.debug("xmpp_join: history_filter = %s", history_filter)
		
		# TODO: factor this part into a subroutine
		#  I dislike all these
		# the only tricky part is deciding where to cut the API: do I pass the <history> stanza in or do I first extract its fields?			
		history = irc.channels[channel].history
		
		# http://xmpp.org/extensions/xep-0045.html#enter-managehistory
		#  <presence from="kousu@galleon/Gajim" id="gajim_muc_670_549748" xml:lang="en" to="#test@conference.irc/kousu"><x xmlns="vcard-temp:x:update" /><c xmlns="http://jabber.org/protocol/caps" node="http://gajim.org" hash="sha-1" ver="OHkcneXc9eqyD5VIJDibu4fR0QY=" /><x xmlns="http://jabber.org/protocol/muc"><history maxstanzas="20" since="2016-03-07T09:32:47Z" /></x></presence>
		# if *not* given, it's up to the MUC to decide what to do: it can send all the history, some of the history, or none
		# we here send all the history we know of, but keep a configurable limit to make sure that that list doesn't explode
		# (we use the same code path in both cases: no history_filter is the same as a history_filter with empty fields)
			# three possible filters on history, one of which comes in two flavours:
		# - maxchars -- by adding up the length of the generated stanzas
		# - maxstanzas -- the limit on the number of stanzas to send
		# - time: -- which can come in *two* attributres: 'seconds' or 'since'; it only makes sense for one to apply at most,
		# but the spec doesn't say which:
		# "The service MUST send the smallest amount of traffic that meets any combination of the above criteria,
		#  taking into account service-level and room-level defaults." 
		# so I've chosen that seconds overrides since
		
		
		maxchars = float("+inf") # default values
		maxstanzas = float("+inf")
		since = float("-inf")
		if history_filter is not None: #apparently bool(stanza) == False, so we can't just "if history_filter"
			if history_filter.get('maxchars'):
				maxchars = int(history_filter.get('maxchars'))
			if history_filter.get('maxstanzas'):
				maxstanzas = int(history_filter.get('maxstanzas'))
			
			if history_filter.get('seconds'):
				# 'seconds' means a number of seconds to look backwards
				# so take the current time, and subtract
				since = int(time()) - int(history_filter.get('seconds'))
			elif history_filter.get('since'):
				# 'since' means an absolute timestamp
				since = datetime_parse(history_filter.get('since')).timestamp()
			
		def irc_to_xmpp_history(msg):
			M = self.Message()
			M['to'] = client
			M['from'] = self.JID(channel, msg.nick)
			M['body'] = msg.text
			M['type'] = "groupchat"
			M.append(ET.Element("delay", {'xmlns': 'urn:xmpp:delay', 'from': self.boundjid.full, 'stamp': timestamp_format(msg.timestamp)}))
			return M
		
		# - conditions are disabled indirectly by setting their numbers to infinity; this means we needn't write special-cases into the code here
		# - we have to reverse the stanzas because maxchars means we cannot possibly know where to cut for sure until we generate *in reverse*we generate them in reverse
		# FUCK
		# I think I need to implement message ID tracking
		# without it, Conversations will just multiply-replay messages
		# DOUBLE FUCK: the server doesn't echo messages back
		# TRIPLE FUCK: /me doesn't work because it's an asshole
		
		def trailing_history_stanzas():
			#logging.debug("trailing_history_stanzas: maxchars=%s, maxstanzas=%s, since=%s", maxchars, maxstanzas, since)
			chars = 0 # count of generated chars (for 'maxchars')
			stanzas = 0
			#logging.debug("trailing_history_stanzas: history=%s", history)
			#logging.debug("range = range(-1, -%d, -1)", min(len(history), maxstanzas))
			# SUBTLETY: you can't pass a float to range(), but len(history) is a) always integer b) always <= float("+inf"), so if maxstanas is +inf (i.e. disabled) it will be *ignored*
			for i in range(-1, -(min(len(history), maxstanzas) + 1), -1): # count backwards from the end of the history list: -1, -2, -3, ...; the +1 is because when going backwards in python there's an off-by-one issue (e.g. len(range(-1, -20, -1)) == 19, when you probably wanted 20)
				msg = history[i]
				logging.debug("considering history[%d]=%s", i, msg)
				if(chars > maxchars):
					logging.debug("cutting history due to overflowing maxchars=%d with chars=%d", maxchars, chars)
					break
				if msg.timestamp < since:
					logging.debug("cutting history due to overflowing since=%s with msg.timestamp=%s", since, msg.timestamp)
					break # ASSUMPTION: history is sorted by timestamp
							
				msg = irc_to_xmpp_history(msg)
				chars += len(str(msg)) #XXX this isn't totally correct: this doesn't account for the effect of sanitize_xml
				 # but as the maxchars is just an upper limit picked semi-arbitrarily by the client, *WHATEVER* 
				
				yield msg
		history = reversed(list(trailing_history_stanzas()))
		for M in history:
			M.send()
				

		
		# 4) send live messages
		# Technically we're not allowed to send live messages until we send presence, subject, and history
		# most clients seem to either ignore or display these, so it's really not a big deal.
		# We enforce it by removing the client's resource from the set of active resources
		
		self.roster[room][client.bare].resources[client.resource] = _client



	def on_got_offline(self, stanza):
		# this is *to a room*
		irc = self.bridge.irc[stanza['from']]
		r = MUCJID(stanza['to'])
		channel = ircaddr(MUCJID(r.bare))
		
		pass

		# XXX if you were be a faithful bridge, then parting a room in XMPP should part you from the room in IRC
		# and you would do that here
		#irc.part(channel)
		# However, people get pissed off at you when you join and part and join and part, because their clients are dumb
		# and there's no way to get room history if you IRC part a room
		# so instead we *stay in*
		# TODO: offer a /part command to actually leave a room
	
	# Messaging
	def on_message(self, stanza):
		if stanza['type']:
			self.event('message_%(type)s' % stanza, stanza)
	
	def on_subject(self, stanza):
		irc = self.bridge.irc[stanza['from']]
		channel = ircaddr(MUCJID(stanza['to'].bare))
		irc.topic(channel, " ".join(lines(stanza['subject']))) # Notice: this flattens the subject to a single line
	
	
	def on_message_groupchat(self, stanza):
	
		if (stanza['body'].startswith('/') and not stanza['body'].startswith('/me ')) or not stanza['to'].user: # the 'and not' is to because XEP 0245 is special #XXX TODO: just roll XEP 0245 into the CLI and be done with it
			# command! pass to the CLI
			if stanza['body'].startswith('/'):
				stanza['body'] = stanza['body'].replace("/","",1)
			self.bridge.CLI(stanza)
			return
	
		irc = self.bridge.irc[stanza['from']]
		
		channel = ircaddr(MUCJID(stanza['to'].bare))
		msg = stanza['body']

		irc.send(channel,msg)

		# we have to echo the message back
		# both IRC and XMPP assume that in groupchat assume it's up to the server to state
		# IRC: never echoes your own messages, either in PM or in channels
		# XMPP: does not echo <message type='chat'> (ignoring Carbon/MAM/the other experimental specs for relaying to your own other devices), echoes <message type='groupchat'>
		# the most reliable way to do this is to fake an *irc* event coming from ourselves
		# since then our own handlers will kick in and do the translation
		import irc as libirc # kludge!!!
		irc.event('pubmsg', libirc.client.Event('pubmsg', irc.real_nickname, channel, [msg]))
	
	def on_message_chat(self, stanza):
		# this is a room-mediated PM
		# as such, ignroe everything except for the sender and receiver
		
		# but: we should warn people not to use this address?
		# or maybe we should just disallow this address
		
		stanza.reply("Error: %(whisper)s is a room-mediated whisper address. IRC does support not support this kind of message: all whispers are sent at the server, not room, level.\n"
			     " Your client %(client)s was told that this room is non-anonymous, and therefore should have known to use %(PM)s.\n"
			     " If your client brought you here it has a bug." %
		             {"whisper": stanza['to'], "client": stanza['from'].resource, "PM": self.bridge.users.JID(MUCJID(stanza['to']).nick)}).send()


###############################################


class UsersHandler(AutoregisterEventHandlers, ComponentXMPP_, ):
	def __init__(self, bridge, *args, **kwargs):
		super().__init__(*args, **kwargs)
		self.bridge = bridge

		self.auto_authorize = True
		
		# XXX this doesn't work unless your domain is a subdomain of the vhost
		# clients don't know to query for it
		self.register_plugin('xep_0030')
		self['xep_0030'].add_identity(category='gateway',
                              itype='irc333', #Gajim (and soon, Conversations, thanks to @singpolyma) will display this as the "type" of the account to add to your XMPP roster, which is about as close to seamless as this is going to get. Our type, clearly, is IRC
			        # yes, that's a unicode arrow to represent bridging
                              name='jitoi: xmpp://%s ⥈ irc%s://%s:%d' % (self.boundjid, "s" if self.bridge.irc_server[2] else "", self.bridge.irc_server[0], self.bridge.irc_server[1]),
                              jid=self.boundjid,
                              lang='no')
		self['xep_0030'].add_item(self.boundjid, ijid=self.boundjid)
		
		## the default <presence type='probe'> handler is mostly alright
		## but it has the nuisance of *sending a plain <presence>* immediately if you're subscribed
		## but this doesn't reflect the real state. We use IRCHandler._probe_presence to find that.
		## so disable the default.
		## really, I want to disable rosteritem._handle_probe, but that's buried so many layers deep and I can't just swap out what class, can I?
		##self.del_event_handler('presence_probe', self._handle_probe)


	## Session start/end: on the connection of the component to the server
	
	def on_session_start(self, *args):
		# you'd think this would relay a <presence from='irc.transport'> to [<presence from='irc.transport' to=JID> for JID in accounts if 'irc.transport' in accounts[JID].roster],
		# but instead the server returns an error, because there's no 'to' attached
		# argh, why are clients allowed to send broadcast presences but not servers??
		# ..maybe we are. maybe this is a bug in prosody? but ejabberd did this too, I think.
		
		# Since we don't have the roster loaded until we sniff it from users sending *us* presence, so we can't fill in the 'to' addresses.
		#self.xmpp.send_presence()
		pass
	
	def on_session_end(self, _):
		" we (i.e. the component) got kicked off the server for some reason, which we aren't told "
		#XXX there's also on_xmpp_disonnected. should we do that one first??
		# XXX __exit__ sends XMPP messages, and that happens after this. we need to rearrange so that the XMPP disconnect presence stanzas get sent earlier and in such a way that they are only
		# ... but we also want to send them on ctrl-c.. hmmm..... 
		assert _ == {}, "??? what the frell is _=%r this??" % (_,)
		
		# drop all active IRC connections
		# Beware: the dictionary shrinks during iteration
		# Also: buried inside of this are "unavailable" presences for everyone and every *room*
		for jid in list(self.bridge.irc):
			self.bridge.disconnect_for(jid, "Transport shutdown")
		
		logging.debug("Offlining transport to everyone.")
		# - setting the transport itself unavailable to everyone 
		for me in self.roster[self.boundjid]:
			self.send_presence(pto=me, pfrom=self.JID(), ptype="unavailable")
		
		self.bridge.channels.disconnect() #be aware: this will trigger a second "Event triggered: session_end", but it's for the /other/ component.
		self.loop.stop()
	
	
	## Presence
	
	def on_presence(self, stanza):
		"Handler for any <presence> stanza."
		
		# If we get any presence from someone *except* for subscription <presence>s,
		# we can infer that we're on their roster, so record that stanza['from'] is sharing presence with stanza['to'].
		# (which, if auto_authorize is on, should always true: by definition that means every JID
		#  the transport promiscuously accepts presence from anyone outside the transport)
		#
		# This has to be done manually because where the Slixmpp roster system expects that you will persist it
		# to a database of some sort we're just implying rosters from the <presence> stanzas we see
		# (because dealing with disk, permissions, and database mock objects is a huge headache, for testing *and* for my users).
		# Everytime the bridge is rebooted Slixmpp reconstructs the roster by sniffing <presence> stanzas
		# but it *doesn't* set the 'from' and 'to' properties. It only sets those when it sees <presence type="subscribe(d)?">.
		#
		# This is necessary to prevent auto_authorize from immediately unsubscribing e.g. nick@kousu.ca from user@irc.kousu.ca:
		# When you auto_authorize you get roster[stanza['to']][stanza['from']]['from'] = roster[stanza['to']][stanza['from']]['to'] = True,
		# and then on presence_probe slixmpp.roster.item.RosterItem.handle_probe() knows 
		# > I am expecting this <presence> because I have this person's 'from' recorded.
		# If it *doesn't* have that 'from' recorded, it sends <presence type="unsubscribe"> back to tell you to fuck off.
		#
		# The roster datastructure, which is a faithful representation of the subscription spec, reads strangely.
		#  roster[A][B]['to']   says that "B sends presence to A", or "A is subscribed to B"
		#  roster[A][B]['from'] says that "B gets presence from A"
		#
		# This is the longest comment for three lines of code I have written in a while.
		 
		if "subscribe" not in stanza['type']: # the four kinds of subscription stanzas: <presence type="(un)?subscribe(d)?">

			self.roster[stanza['to']][stanza['from']]['from'] = True
			#self.roster[stanza['to']][stanza['from']]['to'] = True
			#self.roster[stanza['from']][stanza['to']]['to'] = True # ???
			#self.roster[stanza['from']][stanza['to']]['from'] = True # ??? #<-- this should be the only true one. "user gets presence from target"
			# but the auto_authorize stuff wants to unsubscribe me if:


	def on_presence_probe(self, stanza):
		if stanza['to'] == self.boundjid:
			# the bridge itself is always online
			# XXX should this maybe be in the parent class?
			r = stanza.reply()
			r['type'] = "available"
			r.send()
	
	# Online/offline: for when a user is (fully) online/offline, we either:
	#  - log them in/out (when they're on/offlining to the transport itself)
	#  - record them in the roster of the bridged user (slixmpp handles this internally)
	#  - record the bridged user in *their* roster, because IRC doesn't have rosters we make it so that if you add a bridged user they appear to add you back
	#    - useful side effect: when auto_authorize is on, a presence_probe will respond by trying to synchronize the internal roster, which means
	#       <presence type='probe' from='nick@kousu.ca' to='saul@irc.kousu.ca'> when roster[nick@kousu.ca][saul@irc.kousu.ca] is missing,
	#       the transport will send back
	#       <presence type='unsubscribe' from='saul@irc.kousu.ca', to='nick@kousu.ca'>
	#      which means that signing in will drop your entire contact list.
	#      But got_online happens before presence_probe, so by adding the symmetric roster here this doesn't happen.
	
	@asyncio.coroutine
	def on_got_online(self, stanza):
		# stanza['to'] is a bridged contact like user@irc.kousu.ca *or* the transport irc.kousu.ca itself
		# stanza['from'] is a real jabber contact, the one using the bridge like nick@kousu.ca
		# 
		# be enthusiastic
		self.bridge.connect_for(stanza['from'])
		irc = self.bridge.irc[stanza['from'].bare]
		yield from irc.ready.wait()
		
			# ... ?

		
	def on_got_offline(self, stanza):
		if stanza['to'] == self.boundjid: # This is directed to the transport itself
			assert stanza['from'].bare in self.bridge.irc, "If we got_offline on XMPP then we should have been online on the IRC side"
			# TODO: if I simply don't disconnect, what happens?
			# expectation: PMs will be bounced (since that's built into my server)
			# 	but channel messages will get lost (since the MUC spec says *each client* has to join a room, which is fucking annoying)
			#	okay so, I think the better way to solve this is to make a mod that lets me send MUC messages to user@server.com and have them queue...
			#	also: XMPP MUCs are heavily into replaying history, and IRC isn't...
			#logging.debug("disconnecting for %(from)s" % stanza)
			#self.bridge.disconnect_for(stanza['from'])
			irc = self.bridge.irc[stanza['from']]
			irc.away("detached")
		else:
			# when someone got_offline means [from][to].resources is empty, i.e. from they have no more resources listening
			# for our purposes
			# 
			assert not self.roster[stanza['to']][stanza['from']].resources
	
	# XMPP <presence> <--> IRC /away
	#
	# there are six kinds of spec'd presences in XMPP:
	#  type="unavailable"
	#  or for type=none or type="available" there's show={"chat", None, "away", "xa", "dnd"}, and 
	# IRC only has three: online, /away, and offline
	# Here I map chat and available to !away
	#        the next three to away
	# and "unavailable" is obviously mapped to offline, but that's handled by on_got_offline which is supported by some Slixmpp architecture which is smart about counting how many unavailables there are
	# I've written them out one by one because there's some subtle UI touches different between them 
	#
	# because XMPP does presence on a peer-to-peer basis and IRC on a peer-to-server basis, we need to flatten the storm of presence updates
	# into just one. We do this by catching the update that's targeted at the transport itself.
	# we could maybe just catch the first presence update sent and then remember what the status and state was ... but this is easier.
	# 
	# TODO: even with the guard, we might accidentally flood the server with /aways. We should check for nowaway, and keep trying until we get it (but be careful that retrying doesn't itself cause a flood!)
	# TODO: it's probably drastically simpler, on account of, to simply do one handler in on_presence which looks at the *roster* object for presence['from']
	# because these current take the latest updated, instead of mushing down stuff
	
		
	# http://tools.ietf.org/html/rfc2812#section-4.1 
	#
	# TODO: we need to take the *highest* presence status across all resources
	# currently this just bounces your status to your *most recent* status, which will almost certainly be your phone if you have one since they change status whenever you turn on/off the screen

	def on_presence_available(self, presence):
		if presence['to'] != self.boundjid: return
		self.bridge.irc[presence['from']].back()
	def on_presence_chat(self, presence):
		if presence['to'] != self.boundjid: return #guard against sending one update per roster element
		self.bridge.irc[presence['from']].back()
	
	def on_presence_away(self, presence):
		if presence['to'] != self.boundjid: return
		self.bridge.irc[presence['from']].away("Away" + ((": %s" % (presence['status'],)) if presence['status'] else ""))
	def on_presence_xa(self, presence):
		if presence['to'] != self.boundjid: return
		self.bridge.irc[presence['from']].away("Extended Away" + ((": %s" % (presence['status'],)) if presence['status'] else ""))
	def on_presence_dnd(self, presence):
		if presence['to'] != self.boundjid: return
		self.bridge.irc[presence['from']].away("Do Not Disturb" + ((": %s" % (presence['status'],)) if presence['status'] else ""))
	
	
		
	## Messaging

	def on_message(self, msg):
		" simply proxy a <message> stanza to its proper subtype handler "
		if msg['type']:
			self.event('message_%(type)s' % msg, msg)
	
	def on_message_chat(self, msg):
		
		assert msg['to'].domain == self.bridge.users.boundjid.domain, "This component should only receive XMPP stanzas destined for itself"

		if (msg['body'].startswith('/') and not msg['body'].startswith('/me ')) or not msg['to'].user: # the 'and not' is to because XEP 0245 is special #XXX TODO: just roll XEP 0245 into the CLI and be done with it
			# command! pass to the CLI
			# a command is *either* a / command from somewhere
			# a message direct to the transport itself
			if msg['body'].startswith('/'):
				msg['body'] = msg['body'].replace("/","",1)
			self.bridge.CLI(msg)
			return
		
		if msg['to'].user:
			irc = self.bridge.irc[msg['from'].bare] # XXX DRY this up
			assert msg['from'].bare == irc.jid
			assert msg['to'].resource == "", "And none of the target endpoints should have /resource parts attached because where would they come from?"
			
			irc.send(msg['to'].user, msg['body'])







class Bridge:
	# members:
	#users = UserComponent() # handlers for stanzas targetted at JIDs
	#channels = ChannelComponent() # handlers for stanzas targetted at MUCs
	#irc[jid] = irc
	def __init__(self, jid, xmppd, secret, irc):
		"""
		...
		...

		irc should be a 3-tuple: (host, port, secure). Secure determines if SSL is used.
		"""
		if not (isinstance(irc[0], str) and isinstance(irc[1], int) and 0<irc[1]<2**16):
			raise TypeError(irc) #TODO: better error
		
		if not isinstance(irc, tuple):
			irc = (irc,)
		if len(irc) == 1:
			irc = (irc[0], 6697, True)
		elif len(irc) == 2:
			irc = (irc[0], irc[1], True)
		self.irc_server = irc

		
		self.CLI = TransportCLI(self)
		self._registrations = {} # map of JID -> irc login details.
					# Right now this is created only at runtime, but in theory we should be persisting it to disk
		
		self.users = UsersHandler(self, jid, secret, xmppd[0], xmppd[1])
		self.channels = ChannelsHandler(self, "conference." + jid, secret, xmppd[0], xmppd[1])
		self.irc = JIDict()
		# sneaky hack: optionally allow indexing bridge.irc by *stanzas* (it looks at the from address,
		# since we're server-side code so the from address is the client we're trying to proxy on behalf of)
		# XXX this totally doesn't work. what's wrong with it?
		self.irc.__getitem__ = lambda self, k: JIDict.__getitem__(self, k) if not isinstance(k, stanza) else JIDict.__getitem__(self, k['from'])
		
		
		
	def connect(self):
		self.users.connect()
		self.channels.connect()
		
		
	
	def disconnect(self):
	
		# go through and terminate IRC connections
		# note: we do this *before* killing XMPP so that IRCHandler is able to send <presence> stanzas offlining
		# ....
		
		# Now disconnect XMPP
		#self.channels.disconnect() # reverse order! take down channels before taking down the transport itself
		self.users.disconnect()
		
	def register(self, jid, handle, pass_=None, user=None, realname="~jitoi"):
		" jid: the JID to register an IRC account for "
		" handle: the nickname to login with "
		" pass_: the server (not room!) password, if required "
		" user: the username to login with, if different from the handle "
		" realname: the irc 'realname' that no one ever uses. "
		
		self._registrations[jid] = (handle, user, pass_, realname)
	
	@catch_and_release
	def connect_for(self, jid):
		logging.debug("connect_for(%r)", jid)
		# XXX this semble it should be in a subclass which is *just* for JID? maybe????
		jid = JID(jid).bare
		if jid in self.irc:
			logging.warn("%s is already connected to %s." % (jid, self.irc[jid].real_server_name))
			return
		
		handle, user, password, realname = self._registrations[jid]
		self.irc[jid] = IRCHandler(self, jid, self.irc_server, handle, password, user, realname)
		self.irc[jid].connect()
		assert self.irc[jid].is_connected()
	
	@catch_and_release
	def connect_pseudoanon(self, jid, nick):
		"kludge:"
		logging.debug("connect_pseudoanon(%r, %r)", jid, nick)
		# XXX this semble it should be in a subclass which is *just* for JID? maybe????
		jid = JID(jid).bare
		if jid in self.irc:
			logging.warn("%s is already connected to %s." % (jid, self.irc[jid].real_server_name))
			return
		
		self.irc[jid] = IRCHandler(self, jid, self.irc_server, nick)
		self.irc[jid].connect()
		assert self.irc[jid].is_connected()

	@catch_and_release
	def disconnect_for(self, jid, reason=""):
		logging.debug("disconnect_for(%r)", jid)
		jid = JID(jid).bare
		if jid not in self.irc:
			return

		irc = self.irc.pop(jid)
		logging.debug("disconnect_for(%r) irc = %s" % (jid, irc))

		if not irc.connected:
			# it's possible to disconnect on a connection already disconnected (e.g. if the other side did it)
			logging.warn("%s is not connected to irc." % (jid,))

		irc.disconnect(reason)
		assert not irc.is_connected()

		
		
		
		
	
	
	### Sugar
	##

	@property
	def loop(self): return self.users.loop

	@property
	def boundjid(self): return self.users.boundjid
	
	def __enter__(self):
		self.connect()
		return self
		
	def __exit__(self, *args):
		self.disconnect()
	
