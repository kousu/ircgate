"""
cli.py: a command line interface for JITOI

Unfortunately, this has to be somewhat hand-rolled
though it would be nice if I would use Click directly.
"""

from slixmpp.stanza import Message
import shlex

from patches import isbridgedircchannel

# a CLI
# TODO: is it worth trying to munge Click to fit this? That could be sweet, but also I think Click makes a lot of assumptions about running on the real command line....
class CLI(object):
	"""
	submethods should return strings
	
	to chain subcommands, instantiate the subcommand CLI in your __init__ and write 
	(no, I don't have fancy syntactic sugar for this yet. dealll with it)

	TODO: it sort of feels like I should curry over the user the CLI is for, for isolation
	so, each user, along with having a private IRC object, gets a private CLI object
	"""
	
	def __init__(self, bridge):
		self.bridge = bridge

	def __call__(self, stanza):
		if not isinstance(stanza, Message):
			raise TypeError
		source = stanza['from'].bare
		target = stanza['to']
		commandline = stanza['body']
		try:
			argv = shlex.split(commandline.strip())
			cmd = argv[0].lower() #commands are case-insensitive (mostly because on phones, autocorrect is a dick)
			
			if cmd.startswith("_"):
				# security: we expose a lot, but only the public callables
				# hopefully this is safe enough??
				raise PermissionError(cmd)
			if not callable(getattr(self, cmd, None)):
				raise ValueError("'%s' is not a command" % (cmd,))
			# call down into the subcommand
			resp = getattr(self, cmd)(source, target, *argv[1:])

		except Exception as exc:
			# wrap errors to something printable
			resp = "%s: %s %s" % (cmd, type(exc).__name__, exc)

		# relay the result
		if resp:
			rstanza = self.bridge.users.Message()
			rstanza['to'] = stanza['from']
			rstanza['from'] = self.bridge.boundjid
			rstanza['type'] = "chat"
			rstanza['body'] = resp
			rstanza.send()



from util import *

import inspect

class TransportCLI(CLI):
	
	def __getattr__(self, cmd):
		"on a bad command, respond with the help automatically"
		return lambda jid, target, *args: "'%s' is not a command\n\n%s" % (cmd,self.help(jid, target))

	def help(self, jid, _, cmd=None):
		"get help"
		if cmd:
			raise NotImplementedError("Help on subcommands doesn't exist yet")
		
		def docs():
			for n in ["register", "connect", "status", "nick", "join", "part", "quit"] + ["shell", "roster"] + ["help"]: #dir(self):
				
				f = getattr(self, n, None)
				if n.startswith("_") or not callable(f): continue
				
				doc = list(lines(f.__doc__))[0] if f.__doc__ else ""
				
				A = inspect.getargspec(f)
				s = -len(A.defaults) if A.defaults else len(A.args)
				args, optionals = A.args[3:s], A.args[s:] # 3: cuts off "self, jid, target"; s splits the optional from the mandatory arguments
				optionals = ["[%s]" % o for o in optionals]
				args = " ".join(args + optionals)
				
				yield "/%(cmd)s %(args)s -- %(doc)s" % {"cmd": n, "args": args, "doc": doc}
		
		return ("Jabber to IRC transport\n" +
		        "-------------------------\n\n" + 
		       "This bridge maps your XMPP account %s to an account on the IRC server %s:%d." 
			   "To you, 'user' on IRC is user@%s, and '#channel' is #channel@%s.\n" % (jid, self.bridge.irc_server[0], self.bridge.irc_server[1], self.bridge.users.boundjid, self.bridge.channels.boundjid)+
		       "Some rare IRC channels begin with a &, but that is illegal in XMPP addresses so instead these are bridged with a $ like $channel@%s.\n" %(self.bridge.channels.boundjid,) +
			   "Your XMPP status is squashed into IRC's single status of either being unaway (Available and Chatty), or away with an optional status message (Away, Extended Away, and Busy); if you are signed in multiple times, the highest level status is taken.\n"
			   "\n"
		       "This bridge also bounces: it stays online so you don't have to. When you go offline your IRC connection is marked /away [detached] instead of being shut down,\n"
		       "and when you leave a room your nick idles in the channel, recording messages to give you scrollback when you rejoin. To actually leave, use /part or /quit.\n\n" 
		       "Commands:\n" +
		       "\n".join(docs()) + 
			"\n\nThese commands may be used with any contact or group chat that goes over the transport, but any output will come back from {0.boundjid}.\n"
			"Some clients have their own set of / commands so you need to escape them with something like '/say /help' or '//help';\n"
			"because of this, you can use these commands without the / when talking directly to {0.boundjid}.".format(self.bridge))
	
	def register(self, jid, _, nick, password=None, user=None, realname="~jitoi"):
		"set the IRC account to login to the server with."
		self.bridge.register(jid, nick, password, user, realname)
		self.bridge.disconnect_for(jid)
		return "Your login information has been updated."
	
	def connect(self, jid, _):
		"connect to your registered IRC account."
		resp = None
		if jid in self.bridge.irc:
			resp = "%s is already connected to %s" % (jid, self.bridge.irc[jid])
		self.bridge.connect_for(jid)
		assert self.bridge.irc[jid].is_connected()
		return resp

	def quit(self, jid, _):
		"disconnect your current IRC session."
		irc = self.bridge.irc[jid] if jid in self.bridge.irc else None
		self.bridge.disconnect_for(jid)
		if irc is not None:
			assert not irc.is_connected() and not irc.connected

	
	def nick(self, jid, _, nick):
		"change your current IRC handle, if connected. This does not change the one recorded with /register."
		self.bridge.irc[jid].nick(nick)

	def join(self, jid, _, channel):
		"join a channel"
		# you should use your client's conference join feature; most clients do not understand being told to join a conference
		# TODO: just run this, and make sure IRC join -> XMPP invite is implemented
		#if not channel.startswith("#"):
		#	raise TypeError("%s: Not an IRC channel." % (channel,))
		irc = self.bridge.irc[jid]
		irc.join(channel)

		
	def part(self, jid, target, channel=None, reason=None):
		"leave a channel"
		if channel is None:
			# work out the channel from where this was sent to
			if isbridgedircchannel(target.user):
				channel = target.user
			else:
				raise ValueError("/part must be given a channel to operate on")
		self.bridge.irc[jid].part(channel, reason)
	
	def status(self, jid, _):
		"see your current IRC connection status, including your current nick, whether you are using TLS, and the channels you are idling in"
		if jid in self.bridge.irc and self.bridge.irc[jid].is_connected():
			irc = self.bridge.irc[jid]
			# SUBTLETY: str(irc) includes irc.real_nickname in it, so we don't mention it here
			self.bridge.users.send_message(jid, "Connected to %s" % (irc,), mfrom=self.bridge.users.JID(), mtype='chat')
			if irc.channels:
				self.bridge.users.send_message(jid, "You are on " + str.join(" ", list(irc.channels)), mfrom=self.bridge.users.JID(), mtype='chat')
			# TODO: relay your away status
		else:
			self.bridge.users.send_message(jid, "Not connected", mfrom=self.bridge.users.JID(), mtype='chat')


	if __debug__:
		def shell(self, jid, _):
			"for DEBUGGING: pause the server with an open python shell in the console. self will point to the cli object, and self.bridge is the interesting point"

			self.bridge.users.send_message(jid, "shell launched, check your console. server is hung until you quit it", mfrom=self.bridge.users.boundjid, mtype='chat')
			import bpython; bpython.embed(locals_=dict(list(globals().items()) + list(locals().items())))
			self.bridge.users.send_message(jid, "server resumed", mfrom=self.bridge.users.boundjid, mtype='chat')
		
		def roster(self, jid, _):
			" DEBUG: show your currently known roster "
			# TODO: show jid's roster, all entries jid has in other rosters (plus the resources JID is known by in there), and do it for conferences too
			return str(list(self.bridge.users.roster[jid]) + list(self.bridge.channels.roster[jid]))
	


class NickServ(CLI): # example
	def register(self, jid, password):
		raise NotImplemented


